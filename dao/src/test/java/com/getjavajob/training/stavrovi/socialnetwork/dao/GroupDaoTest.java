package com.getjavajob.training.stavrovi.socialnetwork.dao;

import com.getjavajob.training.stavrovi.socialnetwork.common.Group;
import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:dao-context-test.xml")
@Sql(value = "classpath:InitTables.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:DropTables.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class GroupDaoTest {
    @Autowired
    private GroupDao dao;

    @Transactional
    @Test
    public void getByIdTest() {
        Group group = dao.getById(1);
        assertEquals(1, group.getId());
        assertEquals("Music", group.getName());
        group = dao.getById(3);
        assertEquals(3, group.getId());
        assertEquals("IT", group.getName());
    }

    @Transactional
    @Test
    public void createTest() {
        Group group = new Group("Nature");
        User testUser = new User(1, "test", "test");
        group.setCreator(testUser);
        Group newGroup = dao.save(group);
        assertEquals("Nature", newGroup.getName());
    }
}