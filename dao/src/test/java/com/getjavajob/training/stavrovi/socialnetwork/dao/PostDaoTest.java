package com.getjavajob.training.stavrovi.socialnetwork.dao;

import com.getjavajob.training.stavrovi.socialnetwork.common.Group;
import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import com.getjavajob.training.stavrovi.socialnetwork.common.post.GroupPost;
import com.getjavajob.training.stavrovi.socialnetwork.common.post.UserPost;
import com.getjavajob.training.stavrovi.socialnetwork.dao.post.GroupPostDao;
import com.getjavajob.training.stavrovi.socialnetwork.dao.post.UserPostDao;
import com.getjavajob.training.stavrovi.socialnetwork.dao.user.UserDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:dao-context-test.xml")
@Sql(value = "classpath:InitTables.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:DropTables.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class PostDaoTest {
    @Autowired
    private UserDao userDao;
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private UserPostDao userPostDao;
    @Autowired
    private GroupPostDao groupPostDao;

    @Transactional
    @Test
    public void addUserPostTest() {
        User user = userDao.getById(1);
        User recipient = userDao.getById(2);
        UserPost post = new UserPost(user, recipient, "test");
        userPostDao.addPost(post);
        List<UserPost> expectedPosts = userPostDao.getPosts(recipient);
        List<UserPost> actual = new ArrayList<>();
        actual.add(post);
        assertEquals(expectedPosts, actual);
        assertNotNull(expectedPosts.get(0).getSendingDate());
    }

    @Transactional
    @Test
    public void addGroupPostTest() {
        User user = userDao.getById(2);
        Group recipient = groupDao.getByName("Music");
        GroupPost post = new GroupPost(user, recipient, "test");
        groupPostDao.addPost(post);
        List<GroupPost> expectedPosts = groupPostDao.getPosts(recipient);
        List<GroupPost> actual = new ArrayList<>();
        actual.add(post);
        assertEquals(expectedPosts, actual);
        assertNotNull(expectedPosts.get(0).getSendingDate());
    }
}
