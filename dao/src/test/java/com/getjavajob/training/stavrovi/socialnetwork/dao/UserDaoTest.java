package com.getjavajob.training.stavrovi.socialnetwork.dao;

import com.getjavajob.training.stavrovi.socialnetwork.common.FriendRequest;
import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import com.getjavajob.training.stavrovi.socialnetwork.dao.user.UserDao;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:dao-context-test.xml")
@Sql(value = "classpath:InitTables.sql", executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = "classpath:DropTables.sql", executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class UserDaoTest {
    @Autowired
    private UserDao userDao;
    @Autowired
    private FriendRequestDao friendRequestDao;

    @Transactional
    @Test
    public void getByIdTest() {
        User user = userDao.getById(1);
        assertEquals(1, user.getId());
        assertEquals("Alexander", user.getName());
        user = userDao.getById(2);
        assertEquals(2, user.getId());
        assertEquals("Michael", user.getName());
        user = userDao.getById(-1);
        assertNull(user);
    }

    @Transactional
    @Test
    public void createTest() {
        User user = new User("Geremy");
        userDao.save(user);
        User testAcc = userDao.getById(6);
        assertEquals(6, testAcc.getId());
        assertEquals("Geremy", testAcc.getName());
    }

    @Transactional
    @Test
    public void updateTest() {
        User user = userDao.getById(3);
        user.setName("Mark");
        user.setFamilyName("Tven");
        userDao.save(user);
        user = userDao.getById(3);
        assertEquals(3, user.getId());
        assertEquals("Mark", user.getName());
        assertEquals("Tven", user.getFamilyName());
    }

    @Transactional
    @Test
    public void getAndSetUserPhonesTest() {
        User user = userDao.getById(1);
        List<String> phones = new ArrayList<>();
        phones.add("111");
        phones.add("222");
        user.setPhones(phones);
        userDao.save(user);
        assertEquals(user.getPhones(), phones);
    }

    @Transactional
    @Test
    public void UserRequestTest() {
        User userFrom = userDao.getById(1);
        User userTo = userDao.getById(2);
        // send request
        friendRequestDao.sendRequest(userFrom, userTo);
        List<FriendRequest> requests = userTo.getRequestsToMe();
        assertEquals(requests.get(0).getUserFrom(), friendRequestDao.getUsersFriendRequests(userTo).get(0));
        // accept
        friendRequestDao.acceptFriendRequest(userFrom, userTo);
        // remove request
        friendRequestDao.declineFriendRequest(userFrom, userTo);
        assertEquals(userTo.getRequestsToMe().isEmpty(), true);
    }
}