package com.getjavajob.training.stavrovi.socialnetwork.dao;

import com.getjavajob.training.stavrovi.socialnetwork.common.Group;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupDao extends JpaRepository<Group, Integer> {

    Group getById(Integer id);

    Group getByName(String name);

    @Query("SELECT p FROM Group p WHERE p.name like :query%")
    List<Group> getAllGroupsByQuery(String query);

    @Query("SELECT p FROM Group p WHERE p.name like :query%")
    List<Group> getGroupsByQuery(String query, Pageable pageable);

}
