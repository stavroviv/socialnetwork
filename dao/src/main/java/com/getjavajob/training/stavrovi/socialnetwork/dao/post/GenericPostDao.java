package com.getjavajob.training.stavrovi.socialnetwork.dao.post;

import com.getjavajob.training.stavrovi.socialnetwork.common.BaseEntity;
import com.getjavajob.training.stavrovi.socialnetwork.common.post.GenericPost;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public abstract class GenericPostDao<E extends BaseEntity, T extends GenericPost<E>> {
    private Class<T> entityClass;
    @PersistenceContext
    private EntityManager entityManager;

    public GenericPostDao(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public boolean addPost(GenericPost<E> post) {
        entityManager.persist(post);
        return true;
    }

    public List<T> getPosts(E recipient) {
        return entityManager
                .createQuery("SELECT p FROM " + entityClass.getName()
                        + " p where p.recipient = :recipient"
                        + " order by p.sendingDate desc", entityClass)
                .setParameter("recipient", recipient)
                .getResultList();
    }
}
