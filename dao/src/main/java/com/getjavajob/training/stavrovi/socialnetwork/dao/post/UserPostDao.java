package com.getjavajob.training.stavrovi.socialnetwork.dao.post;

import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import com.getjavajob.training.stavrovi.socialnetwork.common.post.UserPost;
import org.springframework.stereotype.Repository;

@Repository
public class UserPostDao extends GenericPostDao<User, UserPost> {
    public UserPostDao() {
        super(UserPost.class);
    }
}
