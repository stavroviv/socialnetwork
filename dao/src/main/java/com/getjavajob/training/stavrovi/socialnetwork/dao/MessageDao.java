package com.getjavajob.training.stavrovi.socialnetwork.dao;

import com.getjavajob.training.stavrovi.socialnetwork.common.Message;
import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

@Repository
public class MessageDao {
    @PersistenceContext
    private EntityManager entityManager;

    public void sendMessage(User userFrom, User userTo, String text, Date date) {
        Message newMessage = new Message(userFrom, userTo, text);
        entityManager.persist(newMessage);
    }

    public List<Message> getMessages(User userFrom, User userTo) {
        List<Message> messagesFrom = entityManager
                .createQuery("SELECT p FROM Message p " +
                        " where p.recipient = :recipient and p.sender = :sender" +
                        " order by p.sendingDate desc", Message.class)
                .setParameter("recipient", userFrom)
                .setParameter("sender", userTo)
                .getResultList();
        List<Message> messagesTo = entityManager
                .createQuery("SELECT p FROM Message p " +
                        " where p.recipient = :recipient and p.sender = :sender" +
                        " order by p.sendingDate desc", Message.class)
                .setParameter("recipient", userTo)
                .setParameter("sender", userFrom)
                .getResultList();
        List<Message> messages = new ArrayList<>();
        messages.addAll(messagesFrom);
        messages.addAll(messagesTo);
        messages.sort(Comparator.comparing(Message::getSendingDate));
        return messages;
    }
}
