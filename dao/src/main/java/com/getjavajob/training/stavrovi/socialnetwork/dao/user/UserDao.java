package com.getjavajob.training.stavrovi.socialnetwork.dao.user;

import com.getjavajob.training.stavrovi.socialnetwork.common.Group;
import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserDao extends JpaRepository<User, Integer>, CustomUserDao {

    User getByEmail(String email);

    User getById(Integer id);

    @Query("SELECT p FROM User p WHERE p.name like :query% or p.familyName like :query% or p.middleName like :query%")
    List<User> getAllUsersByQuery(String query);

    @Query("SELECT p FROM User p WHERE p.name like :query% or p.familyName like :query% or p.middleName like :query%")
    List<User> getUsersByQuery(String query, Pageable pageable);

    @Query("SELECT p FROM Group p WHERE p.creator = :user")
    List<Group> getUsersGroupsByOwner(User user);

}
