package com.getjavajob.training.stavrovi.socialnetwork.dao;

import com.getjavajob.training.stavrovi.socialnetwork.common.FriendRequest;
import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@Repository
public class FriendRequestDao {
    @PersistenceContext
    private EntityManager entityManager;

    public void sendRequest(User from, User to) {
        FriendRequest newRequest = new FriendRequest(from, to);
        entityManager.persist(newRequest);
    }

    public List<User> getUsersFriendRequests(User user) {
        List<FriendRequest> friendRequests = entityManager
                .createQuery("SELECT p FROM FriendRequest p WHERE p.userTo = :user and p.accepted=false",
                        FriendRequest.class)
                .setParameter("user", user)
                .getResultList();
        List<User> users = new ArrayList<>();
        friendRequests.forEach(request -> users.add(request.getUserFrom()));
        return users;
    }

    public void acceptFriendRequest(User userFrom, User userTo) {
        getFriendRequest(userFrom, userTo).setAccepted(true);
    }

    public void declineFriendRequest(User userFrom, User userTo) {
        userTo.removeRequest(getFriendRequest(userFrom, userTo));
    }

    public FriendRequest getFriendRequest(User userFrom, User userTo) {
        try {
            return entityManager
                    .createQuery("SELECT p FROM FriendRequest p " +
                            "WHERE p.userTo = :userTo and p.userFrom = :userFrom", FriendRequest.class)
                    .setParameter("userFrom", userFrom)
                    .setParameter("userTo", userTo)
                    .getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }
}
