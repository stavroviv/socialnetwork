package com.getjavajob.training.stavrovi.socialnetwork.dao;

public class DaoException extends Exception {

    public DaoException(String message, Throwable cause) {
        super(message, cause);
    }

}
