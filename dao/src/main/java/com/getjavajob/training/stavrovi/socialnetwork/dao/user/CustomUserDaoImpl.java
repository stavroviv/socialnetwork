package com.getjavajob.training.stavrovi.socialnetwork.dao.user;

import com.getjavajob.training.stavrovi.socialnetwork.common.FriendRequest;
import com.getjavajob.training.stavrovi.socialnetwork.common.User;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

public class CustomUserDaoImpl implements CustomUserDao {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<User> getFriends(User user) {
        // SQL supports UNION, but JPA 2.0 JPQL does not
        List<FriendRequest> friendRequestsFrom = entityManager
                .createQuery("SELECT p FROM FriendRequest p WHERE p.userFrom = :user and p.accepted=true",
                        FriendRequest.class)
                .setParameter("user", user)
                .getResultList();
        List<FriendRequest> friendRequestsTo = entityManager
                .createQuery("SELECT p FROM FriendRequest p WHERE p.userTo = :user and p.accepted=true",
                        FriendRequest.class)
                .setParameter("user", user)
                .getResultList();
        List<User> users = new ArrayList<>();
        friendRequestsFrom.forEach(request -> users.add(request.getUserTo()));
        friendRequestsTo.forEach(request -> users.add(request.getUserFrom()));
        return users;
    }
}
