package com.getjavajob.training.stavrovi.socialnetwork.dao.post;

import com.getjavajob.training.stavrovi.socialnetwork.common.Group;
import com.getjavajob.training.stavrovi.socialnetwork.common.post.GroupPost;
import org.springframework.stereotype.Repository;

@Repository
public class GroupPostDao extends GenericPostDao<Group, GroupPost> {

    public GroupPostDao() {
        super(GroupPost.class);
    }
}
