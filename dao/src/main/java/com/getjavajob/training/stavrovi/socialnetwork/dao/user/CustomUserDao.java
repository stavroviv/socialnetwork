package com.getjavajob.training.stavrovi.socialnetwork.dao.user;

import com.getjavajob.training.stavrovi.socialnetwork.common.User;

import java.util.List;

public interface CustomUserDao {

    List<User> getFriends(User user);

}
