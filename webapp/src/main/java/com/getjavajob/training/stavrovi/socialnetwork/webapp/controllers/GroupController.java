package com.getjavajob.training.stavrovi.socialnetwork.webapp.controllers;

import com.getjavajob.training.stavrovi.socialnetwork.common.Group;
import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import com.getjavajob.training.stavrovi.socialnetwork.common.post.GroupPost;
import com.getjavajob.training.stavrovi.socialnetwork.service.GroupService;
import com.getjavajob.training.stavrovi.socialnetwork.service.ServiceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.getjavajob.training.stavrovi.socialnetwork.webapp.utils.Utils.getCurrentUser;
import static com.getjavajob.training.stavrovi.socialnetwork.webapp.utils.Utils.processException;

@Controller
public class GroupController extends HttpServlet {
    private static final Logger errorLog = LoggerFactory.getLogger("ErrorLogger");
    @Autowired
    private GroupService groupService;

    @RequestMapping(value = "/group/{id}/uploadImage", method = RequestMethod.POST)
    public String uploadImage(@PathVariable int id,
                              @RequestParam(required = false, value = "file") MultipartFile photoFile) throws IOException {
        setClearPhoto(id, photoFile.getBytes());
        return "redirect:/group/" + id + "/home";
    }

    @RequestMapping(value = "/group/{id}/clearImage", method = RequestMethod.POST)
    public String clearImage(@PathVariable int id) {
        setClearPhoto(id, new byte[]{});
        return "redirect:/group/" + id + "/home";
    }

    private void setClearPhoto(int id, byte[] photo) {
        Group group = groupService.getGroupById(id);
        group.setPicture(photo);
        try {
            groupService.createOrUpdate(group);
        } catch (ServiceException e) {
            errorLog.error("Error in clear photo", e);
        }
    }

    @RequestMapping(value = "/group/{id}/home")
    public ModelAndView homePage(@PathVariable int id) {
        Group currentGroup = groupService.getGroupById(id);
        ModelAndView modelAndView = new ModelAndView("group");
        modelAndView.addObject("group", currentGroup);
        modelAndView.addObject("posts", groupService.getPosts(currentGroup));
        List<User> members = groupService.getGroupMembers(currentGroup);
        modelAndView.addObject("members", members);
        modelAndView.addObject("isFollower", members.contains(getCurrentUser()));
        modelAndView.addObject("postOnWallMode", "group");
        return modelAndView;
    }

    @RequestMapping(value = "/group_create")
    public ModelAndView createGroupPage() {
        ModelAndView modelAndView = new ModelAndView("groupSettings");
        modelAndView.addObject("pageHeader", "Create");
        return modelAndView;
    }

    @RequestMapping(value = "/group/{id}/edit")
    public ModelAndView modifyGroupPage(@PathVariable int id) {
        ModelAndView modelAndView = new ModelAndView("groupSettings");
        modelAndView.addObject("pageHeader", "Edit");
        modelAndView.addObject("group", groupService.getGroupById(id));
        return modelAndView;
    }

    @RequestMapping(value = "/group/{id}/saveSettings")
    public String saveSettings(@PathVariable int id,
                               @RequestParam(value = "name") String name,
                               @RequestParam(value = "addInfo") String addInfo,
                               RedirectAttributes redirectAttributes) {
        String result;
        Group group = id == 0 ? new Group() : groupService.getGroupById(id);
        group.setName(name);
        group.setAddInfo(addInfo);
        if (id == 0) {
            try {
                group.setCreator(getCurrentUser());
                groupService.createOrUpdate(group);
            } catch (ServiceException e) {
                e.printStackTrace();
            }
            int newId = groupService.getGroupByName(group.getName()).getId();
            result = "redirect:/group/" + newId + "/home";
        } else {
            try {
                groupService.createOrUpdate(group);
                redirectAttributes.addFlashAttribute("message", "Changes saved. Group settings has been successfully updated.");
            } catch (Exception e) {
                processException(e, redirectAttributes);
            }
            result = "redirect:/group/" + id + "/edit";
        }
        return result;
    }

    @RequestMapping(value = "/group/{id}/sendPost", method = RequestMethod.POST)
    public String sendPost(@PathVariable int id,
                           @RequestParam(value = "postText") String postText,
                           HttpSession session) {
        User sender = (User) session.getAttribute("currentUser");
        Group recipient = groupService.getGroupById(id);
        groupService.addPost(new GroupPost(sender, recipient, postText));
        return "redirect:/group/" + id + "/home";
    }

    @RequestMapping(value = "/group/{id}/followGroup", method = RequestMethod.GET)
    public String followGroup(@PathVariable int id) {
        groupService.addMember(id, getCurrentUser());
        return "redirect:/group/" + id + "/home";
    }

    @RequestMapping(value = "/group/{id}/unfollowGroup", method = RequestMethod.GET)
    public String unfollowGroup(@PathVariable int id) {
        groupService.removeMember(id, getCurrentUser());
        return "redirect:/user/" + getCurrentUser().getId() + "/home";
    }
}
