package com.getjavajob.training.stavrovi.socialnetwork.webapp.utils;

import com.getjavajob.training.stavrovi.socialnetwork.common.JmsMessage;
import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import com.getjavajob.training.stavrovi.socialnetwork.service.security.UserDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import static com.getjavajob.training.stavrovi.socialnetwork.webapp.jms.ActiveMqConfig.ADMIN_MESSAGE_QUEUE;
import static com.getjavajob.training.stavrovi.socialnetwork.webapp.jms.ActiveMqConfig.USER_MESSAGE_QUEUE;

@Component
public class Utils {
    private static final Logger errorLog = LoggerFactory.getLogger("ErrorLogger");
    private static JmsTemplate jmsTemplate;

    public static void processException(Exception e, RedirectAttributes redirectAttributes) {
        redirectAttributes.addFlashAttribute("error", e.getClass());
        redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
        errorLog.error("DAO exception", e);
    }

    @Autowired
    public void setAutowiredFields(JmsTemplate jmsTemplate) {
        Utils.jmsTemplate = jmsTemplate;
    }

    public static User getCurrentUser() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth instanceof AnonymousAuthenticationToken ? null : ((UserDTO) auth.getPrincipal()).getUser();
    }

    public static void sendJmsMessage(String text) {
        JmsMessage newMessage = new JmsMessage(text);
        newMessage.setQueueName(ADMIN_MESSAGE_QUEUE);
        try {
            jmsTemplate.convertAndSend(ADMIN_MESSAGE_QUEUE, newMessage);
        } catch (Exception e) {
            errorLog.error("Send JMS messages error", e);
        }
    }

    public static void sendJmsMessageToUser(String text, String userTo) {
        JmsMessage newMessage = new JmsMessage(text);
        newMessage.setQueueName(USER_MESSAGE_QUEUE);
        newMessage.setUserTo(userTo);
        try {
            jmsTemplate.convertAndSend(USER_MESSAGE_QUEUE, newMessage);
        } catch (Exception e) {
            errorLog.error("Send JMS messages error", e);
        }
    }
}
