package com.getjavajob.training.stavrovi.socialnetwork.webapp.controllers;

import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import com.getjavajob.training.stavrovi.socialnetwork.common.UserRole;
import com.getjavajob.training.stavrovi.socialnetwork.common.post.UserPost;
import com.getjavajob.training.stavrovi.socialnetwork.service.GroupService;
import com.getjavajob.training.stavrovi.socialnetwork.service.ServiceException;
import com.getjavajob.training.stavrovi.socialnetwork.service.UserService;
import com.getjavajob.training.stavrovi.socialnetwork.webapp.utils.Utils;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.StaxDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.getjavajob.training.stavrovi.socialnetwork.webapp.utils.Utils.*;

@Controller
public class UserController {
    private static final Logger errorLog = LoggerFactory.getLogger("ErrorLogger");
    @Autowired
    private UserService userService;
    @Autowired
    private GroupService groupService;

    @RequestMapping(value = "/user/{id}/home", method = RequestMethod.GET)
    public ModelAndView homePage(@PathVariable int id, HttpSession session) {
        ModelAndView modelAndView = new ModelAndView("home");
        User user = userService.getUserById(id);
        modelAndView.addObject("user", user);
        modelAndView.addObject("phones", userService.getUserPhonesToString(user));
        modelAndView.addObject("posts", userService.getPosts(user));
        modelAndView.addObject("postOnWallMode", "user");
        User currentUser = (User) session.getAttribute("currentUser");
        if (currentUser == null) {
            currentUser = getCurrentUser();
            session.setAttribute("currentUser", currentUser);
        }
        modelAndView.addObject("isFriend", userService.getFriends(currentUser).contains(user));
        modelAndView.addObject("requestAlreadySent", userService.getUsersFriendRequests(user).contains(currentUser));
        return modelAndView;
    }

    @PreAuthorize("@permissionEvaluator.hasPermission(#id, principal)")
    @RequestMapping(value = "/user/{id}/friends")
    public ModelAndView friendsPage(@PathVariable int id) {
        User currentUser = userService.getUserById(id);
        ModelAndView modelAndView = new ModelAndView("friends");
        modelAndView.addObject("users", userService.getFriends(currentUser));
        modelAndView.addObject("usersFriendRequests", userService.getUsersFriendRequests(currentUser));
        return modelAndView;
    }

    @PreAuthorize("@permissionEvaluator.hasPermission(#id, principal)")
    @RequestMapping(value = "/user/{id}/messages")
    public ModelAndView messagesPage(@PathVariable int id, @RequestParam(required = false) Integer userTo) {
        User currentUser = userService.getUserById(id);
        ModelAndView modelAndView = new ModelAndView("messages");
        modelAndView.addObject("friends", userService.getFriends(currentUser));
        modelAndView.addObject("userTo", userTo == null ? 0 : userTo);
        return modelAndView;
    }

    @PreAuthorize("@permissionEvaluator.hasPermission(#id, principal)")
    @RequestMapping(value = "/user/{id}/groups", method = RequestMethod.GET)
    public ModelAndView groupsPage(@PathVariable int id) {
        ModelAndView modelAndView = new ModelAndView("groups");
        modelAndView.addObject("groups", groupService.getUserGroups(getCurrentUser()));
        return modelAndView;
    }

    @RequestMapping(value = {"/login"})
    public String doLogin(HttpSession session) {
        User user = Utils.getCurrentUser();
        if (user != null) {
            session.setAttribute("currentUser", user);
            return "redirect:/user/" + user.getId() + "/home";
        }
        return "login";
    }

    @RequestMapping("/")
    public String index() {
        return "index";
    }

    @PreAuthorize("@permissionEvaluator.hasPermission(#id, principal)")
    @RequestMapping(value = "/user/{id}/edit", method = {RequestMethod.GET, RequestMethod.POST})
    public ModelAndView editPage(@PathVariable int id) {
        ModelAndView modelAndView = new ModelAndView("settings");
        User currentUser = userService.getUserById(id);
        modelAndView.addObject("user", currentUser);
        modelAndView.addObject("phones", currentUser.getPhones());
        return modelAndView;
    }

    @PreAuthorize("@permissionEvaluator.hasPermission(#id, principal)")
    @RequestMapping(value = "/user/{id}/saveSettings", method = RequestMethod.POST)
    public String saveSettings(@PathVariable int id,
                               @ModelAttribute User userSettings,
                               @RequestParam(required = false, value = "phones") List<String> phonesSettings,
                               HttpSession session, RedirectAttributes redirectAttributes) {
        User user = userService.getUserById(id);
        user.setName(userSettings.getName());
        user.setFamilyName(userSettings.getFamilyName());
        user.setMiddleName(userSettings.getMiddleName());
        user.setSkype(userSettings.getSkype());
        user.setIcq(userSettings.getIcq());
        user.setEmail(userSettings.getEmail());
        user.setBirthDay(userSettings.getBirthDay());
        user.setPhones(phonesSettings);
        try {
            userService.createOrUpdate(user);
            if (user.equals(session.getAttribute("currentUser"))) {
                session.setAttribute("currentUser", user);
            }
            redirectAttributes.addFlashAttribute("message", "Changes saved. Profile has been successfully updated.");
            sendJmsMessage("User " + user + " saved new settings");
        } catch (Exception e) {
            processException(e, redirectAttributes);
            errorLog.error("Error while saving user settings", e);
        }
        return "redirect:/user/" + id + "/edit";
    }

    @RequestMapping(value = "/user/{id}/sendPost", method = RequestMethod.POST)
    public String sendPost(@PathVariable int id,
                           @RequestParam("postText") String postText,
                           HttpSession session) {
        User sender = (User) session.getAttribute("currentUser");
        User recipient = userService.getUserById(id);
        userService.addPost(new UserPost(sender, recipient, postText));
        return "redirect:/user/" + id + "/home";
    }

    @RequestMapping(value = "/user/{id}/uploadImage", method = RequestMethod.POST)
    public String uploadImage(@PathVariable int id,
                              @RequestParam(required = false, value = "file") MultipartFile photoFile) throws IOException {
        setClearPhoto(id, photoFile.getBytes());
        return "redirect:/user/" + id + "/home";
    }

    @RequestMapping(value = "/user/{id}/clearImage", method = RequestMethod.POST)
    public String clearImage(@PathVariable int id) {
        setClearPhoto(id, new byte[]{});
        return "redirect:/user/" + id + "/home";
    }

    private void setClearPhoto(int id, byte[] photo) {
        User user = userService.getUserById(id);
        user.setPicture(photo);
        try {
            userService.createOrUpdate(user);
        } catch (ServiceException e) {
            errorLog.error("Error in clear photo", e);
        }
    }

    @RequestMapping("/registration")
    public String registrationPage() {
        return "registration";
    }

    @RequestMapping(value = "/doRegistration", method = RequestMethod.POST)
    public String doRegister(@ModelAttribute User userSettings,
                             @RequestParam(value = "photoFile", required = false) MultipartFile photoFile,
                             HttpSession session, HttpServletRequest request) throws IOException, ServiceException {
        userSettings.setPicture(photoFile.getBytes());
        userSettings.setRegistrationDate(new Date());
        if (userSettings.getPersonalPhone() != null) {
            List<String> phones = new ArrayList<>();
            phones.add(userSettings.getPersonalPhone());
            userSettings.setPhones(phones);
        }
        String password = userSettings.getPassword();
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        userSettings.setPassword(encoder.encode(password));
        userSettings.setRole(UserRole.USER); // default
        User newUser = userService.createOrUpdate(userSettings);
        session.setAttribute("currentUser", newUser);
        try {
            request.login(userSettings.getEmail(), password);
            sendJmsMessage("New user " + newUser + " has registered and logged in");
        } catch (ServletException e) {
            e.printStackTrace();
        }
        return "redirect:/user/" + newUser.getId() + "/home";
    }

    // friend requests

    @RequestMapping(value = "/{id}/sendFriendRequest")
    @ResponseBody
    public String sendFriendRequest(@PathVariable int id, HttpSession session) {
        User userFrom = (User) session.getAttribute("currentUser");
        User userTo = userService.getUserById(id);
        userService.sendFriendRequest(userFrom, userTo);
        sendJmsMessageToUser(
                "User " + userFrom.getFullName() + " has sent friend request to you",
                userTo.getEmail());
        return "success";
    }

    @RequestMapping(value = "/{id}/acceptRequest")
    public String acceptFriendRequest(@PathVariable int id, HttpSession session) {
        User userFrom = userService.getUserById(id);
        User userTo = (User) session.getAttribute("currentUser");
        userService.acceptFriendRequest(userFrom, userTo);
        session.setAttribute("currentUser", userService.getUserById(userTo.getId()));
        sendJmsMessageToUser(
                "User " + userTo.getFullName() + " has confirmed your friend request",
                userFrom.getEmail());
        return "redirect:/user/" + userTo.getId() + "/friends";
    }

    @RequestMapping(value = "/{id}/declineRequest")
    public String declineFriendRequest(@PathVariable int id, HttpSession session) {
        User userFrom = userService.getUserById(id);
        User userTo = (User) session.getAttribute("currentUser");
        userService.declineFriendRequest(userFrom, userTo);
        session.setAttribute("currentUser", userService.getUserById(userTo.getId()));
        sendJmsMessageToUser(
                "User " + userTo.getFullName() + " has rejected your friend request",
                userFrom.getEmail());
        return "redirect:/user/" + userTo.getId() + "/friends";
    }

    // XML download, upload

    @PreAuthorize("@permissionEvaluator.hasPermission(#id, principal)")
    @RequestMapping(value = "/user/{id}/saveToXML")
    public HttpEntity<byte[]> saveToXML(@PathVariable int id) {
        // https://stackoverflow.com/a/13120104
        User user = userService.getUserById(id);
        XStream xStream = getXStream();
        byte[] xml = xStream.toXML(user).getBytes(StandardCharsets.UTF_8);

        HttpHeaders header = new HttpHeaders();
        header.setContentType(MediaType.APPLICATION_XML);
        header.set("Content-disposition", "attachment; filename=" + "User_" + id + ".xml");
        header.setContentLength(xml.length);
        return new HttpEntity<>(xml, header);
    }

    @PreAuthorize("@permissionEvaluator.hasPermission(#id, principal)")
    @RequestMapping(value = "/user/{id}/loadFromXML", method = RequestMethod.POST)
    public ModelAndView loadFromXML(@PathVariable int id,
                                    @RequestParam("xmlFile") MultipartFile xmlFile,
                                    RedirectAttributes redirectAttributes) throws IOException {
        ModelAndView modelAndView = new ModelAndView("settings");
        if (xmlFile != null && !xmlFile.isEmpty()) {
            StringBuilder sbXML = new StringBuilder();
            try (BufferedReader reader = new BufferedReader(
                    new InputStreamReader(new ByteArrayInputStream(xmlFile.getBytes())))) {
                while (reader.ready()) {
                    sbXML.append(reader.readLine());
                }
            }
            XStream xStream = getXStream();
            User user;
            try {
                user = (User) xStream.fromXML(sbXML.toString());
            } catch (Exception e) {
                redirectAttributes.addFlashAttribute("error", e.getClass());
                redirectAttributes.addFlashAttribute("errorMessage", e.getMessage());
                errorLog.error("Error in loading user settings from XML", e);
                modelAndView = new ModelAndView("redirect:/user/" + id + "/edit");
                return modelAndView;
            }
            modelAndView.addObject("user", user);
            modelAndView.addObject("phones", user.getPhones());
        }
        return modelAndView;
    }

    private XStream getXStream() {
        XStream xStream = new XStream(new StaxDriver());
        xStream.processAnnotations(User.class);
        return xStream;
    }
}
