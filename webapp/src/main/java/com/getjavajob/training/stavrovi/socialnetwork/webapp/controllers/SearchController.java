package com.getjavajob.training.stavrovi.socialnetwork.webapp.controllers;

import com.getjavajob.training.stavrovi.socialnetwork.service.GroupService;
import com.getjavajob.training.stavrovi.socialnetwork.service.UserService;
import org.cloudinary.json.JSONArray;
import org.cloudinary.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SearchController {
    private static final Logger log = LoggerFactory.getLogger(SearchController.class);
    private static final int PAGE_SIZE = 3;
    @Autowired
    private UserService userService;
    @Autowired
    private GroupService groupService;

    @RequestMapping(value = "/header_search_request",
            method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String processHeaderSearch(@RequestParam("query") String query) {
        log.info("header search: " + query);
        JSONArray result = new JSONArray();
        userService.getAllUsersByQuery(query).forEach(acc -> {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("id", acc.getId());
            jsonObj.put("name", acc.getFullName());
            jsonObj.put("PIC", acc.getBase64Picture());
            jsonObj.put("category", "People");
            result.put(jsonObj);
        });
        groupService.getAllGroupsByQuery(query).forEach(group -> {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("id", group.getId());
            jsonObj.put("name", group.getName());
            jsonObj.put("PIC", group.getBase64Picture());
            jsonObj.put("category", "Communities");
            result.put(jsonObj);
        });
        return result.toString();
    }

    @RequestMapping(value = "/search_results", method = RequestMethod.GET)
    public ModelAndView showSearchResults(@RequestParam("search-query") String query) {
        ModelAndView modelAndView = new ModelAndView("searchResults");
        modelAndView.addObject("searchQuery", query);

        modelAndView.addObject("users", userService.getUsersByQuery(query, 0, PAGE_SIZE));
        modelAndView.addObject("usersPages",
                Math.ceil((double) userService.getAllUsersByQuery(query).size() / PAGE_SIZE));

        modelAndView.addObject("groups", groupService.getGroupsByQuery(query, 0, PAGE_SIZE));
        modelAndView.addObject("groupPages",
                Math.ceil((double) groupService.getAllGroupsByQuery(query).size() / PAGE_SIZE));

        return modelAndView;
    }

    @RequestMapping(value = "/search_request", method = RequestMethod.GET, produces = "application/json; charset=utf-8")
    @ResponseBody
    public String processSearchQuery(@RequestParam("query") String query,
                                     @RequestParam("page") int pageNumber,
                                     @RequestParam("type") String type) {
        log.info("global search: " + query);
        JSONArray result = new JSONArray();
        if ("user".equals(type)) {
            userService.getUsersByQuery(query, pageNumber - 1, PAGE_SIZE).forEach(acc -> {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("id", acc.getId());
                jsonObj.put("name", acc.getFullName());
                jsonObj.put("PIC", acc.getBase64Picture());
                result.put(jsonObj);
            });
        } else {
            groupService.getGroupsByQuery(query, pageNumber - 1, PAGE_SIZE).forEach(group -> {
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("id", group.getId());
                jsonObj.put("name", group.getName());
                jsonObj.put("PIC", group.getBase64Picture());
                result.put(jsonObj);
            });
        }
        return result.toString();
    }
}
