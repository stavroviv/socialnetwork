package com.getjavajob.training.stavrovi.socialnetwork.webapp.messages;

public class Message {
    private String sender;
    private String userTo;
    private String text;
    private String date;

    public Message() {
    }

    public Message(String sender, String text, String date) {
        this.sender = sender;
        this.text = text;
        this.date = date;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getUserTo() {
        return userTo;
    }

    public void setUserTo(String userTo) {
        this.userTo = userTo;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
