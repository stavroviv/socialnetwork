package com.getjavajob.training.stavrovi.socialnetwork.webapp.security;

import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import com.getjavajob.training.stavrovi.socialnetwork.webapp.utils.Utils;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.getjavajob.training.stavrovi.socialnetwork.webapp.utils.Utils.sendJmsMessage;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest req, HttpServletResponse resp,
                                        Authentication authentication) throws IOException {
        User user = Utils.getCurrentUser();
        resp.setStatus(HttpServletResponse.SC_OK);
        if (user != null) {
            req.getSession().setAttribute("currentUser", user);
            sendJmsMessage("User " + user + " logged in");
            resp.sendRedirect("/user/" + user.getId() + "/home");
        }
    }
}