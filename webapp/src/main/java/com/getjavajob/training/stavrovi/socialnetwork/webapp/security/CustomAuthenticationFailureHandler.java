package com.getjavajob.training.stavrovi.socialnetwork.webapp.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.FlashMap;
import org.springframework.web.servlet.FlashMapManager;
import org.springframework.web.servlet.support.SessionFlashMapManager;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.getjavajob.training.stavrovi.socialnetwork.webapp.utils.Utils.sendJmsMessage;

@Component
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler {
    @Override
    public void onAuthenticationFailure(HttpServletRequest req, HttpServletResponse resp,
                                        AuthenticationException exception) throws IOException {
        FlashMap flashMap = new FlashMap();
        flashMap.put("error", "Cannot sign-in (wrong email or password)");
        flashMap.put("username", req.getParameter("username"));
        flashMap.put("password", req.getParameter("password"));
        FlashMapManager flashMapManager = new SessionFlashMapManager();
        flashMapManager.saveOutputFlashMap(flashMap, req, resp);
        resp.sendRedirect("/login");
        sendJmsMessage("Failed user auth, user:" + req.getParameter("username")
                + " pass: " + req.getParameter("password"));
    }
}
