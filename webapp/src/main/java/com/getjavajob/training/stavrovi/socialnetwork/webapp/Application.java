package com.getjavajob.training.stavrovi.socialnetwork.webapp;

import com.getjavajob.training.stavrovi.socialnetwork.webapp.jms.ActiveMqConfig;
import com.getjavajob.training.stavrovi.socialnetwork.webapp.messages.WebSocketConfig;
import com.getjavajob.training.stavrovi.socialnetwork.webapp.security.WebSecurityConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.jms.annotation.EnableJms;

@SpringBootApplication
@EntityScan("com.getjavajob.training.stavrovi.socialnetwork.common")
@EnableJpaRepositories("com.getjavajob.training.stavrovi.socialnetwork.dao")
@ComponentScan("com.getjavajob.training")
@Import({WebSecurityConfig.class, WebSocketConfig.class, ActiveMqConfig.class})
@EnableJms
@EnableMongoRepositories(basePackages = "com.getjavajob.training.stavrovi.socialnetwork.dao")
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
        return application.sources(Application.class);
    }
}