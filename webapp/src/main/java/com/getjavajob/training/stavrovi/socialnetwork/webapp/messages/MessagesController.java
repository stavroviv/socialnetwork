package com.getjavajob.training.stavrovi.socialnetwork.webapp.messages;

import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import com.getjavajob.training.stavrovi.socialnetwork.service.UserService;
import org.cloudinary.json.JSONArray;
import org.cloudinary.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.getjavajob.training.stavrovi.socialnetwork.webapp.utils.Utils.sendJmsMessageToUser;

@Controller
public class MessagesController {
    private String DATE_FORMAT = "d MMMM yyyy 'at' HH:mm:ss";
    @Autowired
    private UserService userService;

    @MessageMapping("/chat-messages/{from}/{to}")
    @SendTo("/message/{from}/{to}")
    public Message sendMessage(Message message) {
        User userFrom = userService.getUserById(Integer.parseInt(message.getSender()));
        User userTo = userService.getUserById(Integer.parseInt(message.getUserTo()));
        Date messageDate = new Date();
        userService.sendMessage(userFrom, userTo, message.getText(), messageDate);
        SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
        sendJmsMessageToUser(
                "User " + userFrom.getFullName() + " has sent you message",
                userTo.getEmail());
        return new Message(userFrom.getFullName(), message.getText(), dateFormat.format(messageDate));
    }

    @RequestMapping(value = "/get-messages", produces = "application/json; charset=utf-8")
    @ResponseBody
    public String getMessages(@RequestParam("userFromId") int userFromId,
                              @RequestParam("userToId") int userToId) {
        User userFrom = userService.getUserById(userFromId);
        User userTo = userService.getUserById(userToId);
        JSONArray result = new JSONArray();
        userService.getMessages(userFrom, userTo).forEach(message -> {
            JSONObject mess = new JSONObject();
            mess.put("text", message.getText());
            mess.put("sender", message.getSender().getFullName());
            SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_FORMAT);
            mess.put("date", dateFormat.format(message.getSendingDate()));
            result.put(mess);
        });
        return result.toString();
    }
}