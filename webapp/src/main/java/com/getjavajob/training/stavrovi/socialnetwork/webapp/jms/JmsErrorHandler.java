package com.getjavajob.training.stavrovi.socialnetwork.webapp.jms;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ErrorHandler;

public class JmsErrorHandler implements ErrorHandler {
    private static final Logger errorLog = LoggerFactory.getLogger("ErrorLogger");

    @Override
    public void handleError(Throwable error) {
        errorLog.error("Error in jms handler", error);
    }
}
