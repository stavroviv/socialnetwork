<%@page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html>
<head>
    <title>Social network</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png" type="image/png"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/home.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <!---- jQuery  UI ------>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-1.12.4.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.12.1/jquery-ui.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.12.1/jquery-ui.min.js"></script>

    <%--validation--%>
    <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="https://unpkg.com/imask"></script>

    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/jquery-ui.css" type="text/css"/>

    <script>var context = '<%=request.getContextPath()%>' + '/resources'</script>
    <script src="https://github.com/mouse0270/bootstrap-notify/releases/download/3.1.3/bootstrap-notify.min.js"></script>

</head>
<body>

<!-- Bootstrap NavBar -->
<nav class="navbar navbar-expand-lg navbar-dark fixed-top">
    <a class="navbar-brand"
       href="/user/${sessionScope.currentUser.getId()}/home">${sessionScope.currentUser.getShortName()}</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse"
            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item active">
                <a class="nav-link" href="#">
                    <span class="sr-only">(current)</span>
                </a>
            </li>
        </ul>

        <form class="my-auto w-50 form-inline" role="search"
              action="${pageContext.request.contextPath}/search_results">
            <input id="autoText" class="form-control mr-sm-2" type="search"
                   name="search-query"
                   id="navBarSearchForm" style="width: 500px!important">
            <button type="submit" class="btn btn-outline-secondary my-2 my-sm-0">
                <span class="fa fa-search"></span>
                Search
            </button>
        </form>
    </div>

</nav>

<!-- NavBar END -->