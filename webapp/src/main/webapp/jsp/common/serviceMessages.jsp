<c:if test="${not empty message}">
    <div class="alert alert-success">
        ${message}
    </div>
</c:if>
<c:if test="${not empty error}">
    <div class="alert alert-danger">
        <h3>${error}</h3>
         ${errorMessage}
    </div>
</c:if>