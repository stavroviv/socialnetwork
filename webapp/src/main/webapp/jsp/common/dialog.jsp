<script>
    function openSaveDialog() {
        if (validate()) {
            $('#save-settings').modal('show');
        } else {
            $('#warning').modal('show');
        }
    }
</script>

<!-- Modal -->
<div class="modal fade" id="save-settings"
     tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary text-white">
                Warning
            </div>
            <div class="modal-body">
                Do you want to save changes?
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary" name="actionName" value="saveSettings">
                    Save changes
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">
                    Cancel
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="warning"
     tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-danger text-white">
                Warning
            </div>
            <div class="modal-body">
                Please check all marked fields and try again
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    OK
                </button>
            </div>
        </div>
    </div>
</div>