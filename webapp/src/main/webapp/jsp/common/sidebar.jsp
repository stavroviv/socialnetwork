<!-- Bootstrap row -->
<div class="row" id="body-row">
    <!-- Sidebar -->
    <div id="sidebar-container" class="sidebar-expanded d-none d-md-block col-2">
        <!-- d-* hiddens the Sidebar in smaller devices. Its itens can be kept on the Navbar 'Menu' -->
        <!-- Bootstrap List Group -->
        <ul class="list-group sticky-top sticky-offset">

            <a href="/user/${sessionScope.currentUser.getId()}/home"
               class="bg-dark list-group-item list-group-item-action">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-home fa-fw mr-3"></span>
                    <span class="menu-collapsed">My Profile</span>
                </div>
            </a>

            <a href="/user/${sessionScope.currentUser.getId()}/friends"
               class="bg-dark list-group-item list-group-item-action">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-users fa-fw mr-3"></span>

                    <span class="menu-collapsed">
                        Friends
                        <c:if test='${sessionScope.currentUser.getRequestsToMe().size() > 0}'>
                            <span class="badge badge-pill badge-primary ml-2">
                                    ${sessionScope.currentUser.getRequestsToMe().size()}
                            </span>
                        </c:if>
                    </span>
                </div>
            </a>

            <a href="/user/${sessionScope.currentUser.getId()}/messages"
               class="bg-dark list-group-item list-group-item-action">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-envelope-o fa-fw mr-3"></span>
                    <span class="menu-collapsed">
                        Messages
                        <!--<span class="badge badge-pill badge-primary ml-2">5</span>-->
                    </span>
                </div>
            </a>

            <a href="/user/${sessionScope.currentUser.getId()}/groups"
               class="bg-dark list-group-item list-group-item-action">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-globe fa-fw mr-3"></span>
                    <span class="menu-collapsed">Communities</span>
                </div>
            </a>

            <!-- Separator with title -->
            <li class="list-group-item sidebar-separator-title text-muted d-flex align-items-center menu-collapsed">
                <!--  <small>OPTIONS</small> -->
            </li>
            <!-- /END Separator -->

            <a href="/logout" class="bg-dark list-group-item list-group-item-action">
                <div class="d-flex w-100 justify-content-start align-items-center">
                    <span class="fa fa-sign-out fa-fw mr-3"></span>
                    <span class="menu-collapsed">Log out</span>
                </div>
            </a>


        </ul>
        <!-- List Group END-->
    </div>
    <!-- sidebar-container END -->
