<a href="/user/${user.getId()}/getMessages">
<div class="card flex-row">

    <div class="row no-gutters">

        <div class="col-auto">

                <c:choose>
                    <c:when test="${user.getPicture() != null}">
                        <img class="rounded-circle"
                             src="data:image/jpg;base64,${user.getBase64Picture()}"
                             width="60" height="60"/>
                    </c:when>
                    <c:otherwise>
                        <img class="rounded-circle"
                             src="${pageContext.request.contextPath}/resources/img/user-default.jpg"
                             width="60" height="60">
                    </c:otherwise>
                </c:choose>

        </div>

        <div class="col">
            <div class="card-block px-2">
                <h1></h1>
                ${user.getName()} ${user.getFamilyName()}
            </div>
        </div>

    </div>

</div>
</a>
<h6></h6>