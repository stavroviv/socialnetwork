<div class="card flex-row flex-wrap">
    <div class="row no-gutters">
        <div class="col-auto">
            <a href="/user/${user.getId()}/home">
                <c:choose>
                    <c:when test="${user.getPicture() != null}">
                        <img class="rounded-circle"
                             src="data:image/jpg;base64,${user.getBase64Picture()}"
                             width="60" height="60"/>
                    </c:when>
                    <c:otherwise>
                        <img class="rounded-circle"
                             src="${pageContext.request.contextPath}/resources/img/user-default.jpg"
                             width="60" height="60">
                    </c:otherwise>
                </c:choose>
            </a>
        </div>
        <div class="col">
            <div class="card-block px-2">
                <h1></h1>
                <a href="/user/${user.getId()}/home">${user.getName()} ${user.getFamilyName()}</a>
                <p class="card-text">
                    <small>
                        ${user.getEmail()}
                    </small>
                    <c:if test='${requestPage}'>
                        <div class="btn-group" role="group">
                            <a class="btn btn-primary" href="/${user.getId()}/acceptRequest"
                               role="button">Accept</a>
                            <a class="btn btn-outline-secondary" href="/${user.getId()}/declineRequest"
                               role="button">Decline</a>
                        </div>
                    </c:if>
                </p>
            </div>
        </div>
    </div>
</div>
<h1></h1>