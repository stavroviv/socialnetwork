<div class="card flex-row flex-wrap">
    <div class="row no-gutters">
        <div class="col-auto">
            <a href="/${pagetype}/${entity.getId()}/home">
            <c:choose>
                <c:when test="${entity.getPicture() != null}">
                    <img class="rounded-circle"
                         src="data:image/jpg;base64,${entity.getBase64Picture()}"
                         width="60" height="60"/>
                </c:when>
                <c:otherwise>
                    <img class="rounded-circle"
                    <c:if test="${pagetype.equals('user')}">
                         src="${pageContext.request.contextPath}/resources/img/user-default.jpg"
                    </c:if>
                    <c:if test="${pagetype.equals('group')}">
                         src="${pageContext.request.contextPath}/resources/img/groups-default.jpg"
                    </c:if>
                         width="60" height="60">
                </c:otherwise>
            </c:choose>
            </a>
        </div>
        <div class="col">
            <div class="card-block px-2">
                <h1></h1>
                <c:if test="${pagetype.equals('user')}">
                    ${entity.getFullName()}
                </c:if>
                <c:if test="${pagetype.equals('group')}">
                    ${entity.getName()}
                </c:if>
            </div>
        </div>
    </div>
</div>
<h1></h1>