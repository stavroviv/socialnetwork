<%@include file="common/header.jsp" %>
<%@include file="common/sidebar.jsp" %>

<!-- MAIN -->
<div class="col py-3">

    <div class="row">
        <div class="col-auto profile-pic">

            <c:set var = "isOwner" value = "${sessionScope.get('currentUser').equals(group.getCreator())}"/>

            <c:choose>
                <c:when test="${group.getPicture() != null}">
                    <image src="data:image/jpg;base64,${group.getBase64Picture()}" width="300" height="300"/>
                </c:when>
                <c:otherwise>
                    <img src="${pageContext.request.contextPath}/resources/img/groups-default.jpg" width="300"
                         height="300">
                </c:otherwise>
            </c:choose>

            <c:if test='${isOwner}'>
                <div class="edit">
                    <i data-toggle="modal" data-target="#updatePhoto" class="fa fa-pencil fa-lg"></i>
                    <i data-toggle="modal" data-target="#clearPhoto" class="fa fa-times fa-lg"></i>
                </div>
            </c:if>

            <div class="container">

                <div class="modal fade" id="updatePhoto" role="dialog">
                    <form action="/group/${group.getId()}/uploadImage" method="post" enctype="multipart/form-data">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    Upload a new photo
                                </div>
                                <div class="modal-body">
                                    <p>Upload a logo picture for your community to make it stand out and look pretty.
                                        <BR> We support JPG, GIF or PNG files.</p>
                                    <input type="file" name="file">
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>

            <div class="container">
                <!-- Modal -->
                <div class="modal fade" id="clearPhoto" role="dialog">
                    <form action="/group/${group.getId()}/clearImage" method="post" enctype="multipart/form-data">
                        <div class="modal-dialog">
                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header bg-primary text-white">
                                    Warning
                                </div>
                                <div class="modal-body">
                                    <p>Are you sure you want to delete this photo?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Delete</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>

            </div>
        </div>

        <div class="col">
            <div class="card-block px-2">
                <div class="card-block px-2">
                    <h1></h1>
                    <h4> ${group.getName()}</h4>
                    ${group.getAddInfo().replaceAll("\\n","<BR>")}
                    <BR>
                    <c:if test='${isOwner}'>
                        <a class="btn btn-primary" href="/group/${group.getId()}/edit" role="button">Edit</a>
                    </c:if>
                    <c:if test='${!isFollower}'>
                        <a class="btn btn-primary" href="/group/${group.getId()}/followGroup" role="button">Follow</a>
                    </c:if>
                    <c:if test='${isFollower&&!isOwner}'>
                        <a class="btn btn-primary" href="/group/${group.getId()}/unfollowGroup" role="button">Unfollow</a>
                    </c:if>
                </div>
            </div>
        </div>
    </div>

    <h1></h1>
    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="postOnWall-tab" data-toggle="tab" href="#postOnWall" role="tab" aria-selected="true">
               Posts
            </a>
            <h1></h1>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="members-tab" data-toggle="tab" href="#members" role="tab" aria-selected="false">
               Members
            </a>
        </li>
    </ul>

    <div class="tab-content">

        <div class="tab-pane fade show active" id="postOnWall" role="tabpanel">
            <h1></h1>
            <%@include file="postsOnWall.jsp" %>
        </div>
        <div class="tab-pane fade" id="members" role="tabpanel">
            <c:forEach var="user" items="${members}">
                <%@include file="common/friendUserCard.jsp" %>
            </c:forEach>
        </div>
    </div>

</div>
<!-- Main Col END -->

<%@include file="common/footer.jsp" %>