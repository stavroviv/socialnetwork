<%@page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>

    <title>Social network: registration</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png" type="image/png"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/signin.css">
    <!---- jQuery  UI ------>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-1.12.4.js" type="text/javascript"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.12.1/jquery-ui.js"></script>
    <script src="${pageContext.request.contextPath}/resources/js/jquery-ui-1.12.1/jquery-ui.min.js"></script>

</head>
<body>

<form class="form-horizontal" action="doRegistration" id="registrationForm" method="post" enctype = "multipart/form-data">

    <div class="py-1 text-center">
        <h4>Registration</h4>
    </div>

    <div class="container">

        <label>First name</label>
        <input type="text" class="form-control" id="name" name="name" maxlength="255"/>
        <div class="invalid-feedback">
            First name is required
        </div>

        <label>Last name</label>
        <input type="text" class="form-control" id="familyName" name="familyName" maxlength="255"/>
        <div class="invalid-feedback">
            Last name is required
        </div>

        <label>Middle name</label>
        <input type="text" class="form-control" name="middleName" maxlength="255"/>

        <div class="mb-3">
            <label>Email</label>
            <input class="form-control" id="email" name="email" placeholder="you@example.com" maxlength="30">
            <div class="invalid-feedback">
                Please enter your e-mail
            </div>
        </div>

        <div class="mb-3">
            <label>Password</label>
            <input type="password" class="form-control" id="password" name="password" maxlength="30">
            <div class="invalid-feedback">
                Please enter password
            </div>
        </div>

        <div class="row">
            <div class="col-4">
                <label>Phone</label>
                <input type="text" class="form-control" name="personalPhone" maxlength="20">
            </div>
            <div class="col-4">
                <label>Skype</label>
                <input type="text" class="form-control" name="skype" maxlength="30">
            </div>

            <div class="col-4">
                <label>ICQ</label>
                <input id ="photo" type="text" class="form-control" name="icq" maxlength="10">
            </div>
        </div>

        <hr class="mb-4">

        <div class="mb-3">
            <label>Photo:</label>
            <input type="file" name="photoFile" >
        </div>

        <div class="mb-3">
            <button type="button"
                    class="btn btn-primary btn-lg btn-block" data-target="#save-settings" onclick="openSaveDialog()">
                Register
            </button>
            <%@include file="common/dialog.jsp" %>
        </div>

        <script>var formName='registration'</script>
        <script src="${pageContext.request.contextPath}/resources/js/validator.js" type="text/javascript"></script>
        <script>
            function openSaveDialog() {
                 if (validate()) {
                     document.getElementById("registrationForm").submit();
                 } else {
                    $('#warning').modal('show');
                 }
            }
        </script>

    </div>

</form>


<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>