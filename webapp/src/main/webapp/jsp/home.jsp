<%@include file="common/header.jsp" %>
<%@include file="common/sidebar.jsp" %>

<!-- MAIN -->
<div class="col py-3">

    <div class="row">

        <div class="col-auto profile-pic">

            <c:choose>
                <c:when test='${user.getPicture() != null}'>
                    <img src="data:image/jpg;base64,${user.getBase64Picture()}" width="300" height="300"/>
                </c:when>
                <c:otherwise>
                    <img src="${pageContext.request.contextPath}/resources/img/user-default.jpg" width="300"
                         height="300">
                </c:otherwise>
            </c:choose>

            <c:if test='${sessionScope.get("currentUser").equals(user)}'>
                <div class="edit">
                    <i data-toggle="modal" data-target="#updatePhoto" class="fa fa-pencil fa-lg"></i>
                    <i data-toggle="modal" data-target="#clearPhoto" class="fa fa-times fa-lg"></i>
                </div>
            </c:if>

            <%--Modal update photo--%>
            <div class="container">
                <div class="modal fade" id="updatePhoto" role="dialog">
                    <form action="/user/${user.getId()}/uploadImage" method="post" enctype="multipart/form-data">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header">
                                    Upload a new photo
                                </div>
                                <div class="modal-body">
                                    <p>Please upload a real photo of yourself so your friends can recognize you.
                                        <BR> We support JPG or PNG files.</p>
                                    <input type="file" name="file" accept=".png, .jpg, .jpeg">
                                </div>
                                <div class="modal-footer">
                                    <button type="submit" class="btn btn-primary">Save</button>
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>

            <%--Modal clear photo--%>
            <div class="container">
                <div class="modal fade" id="clearPhoto" role="dialog">
                    <form action="/user/${user.getId()}/clearImage" method="post" enctype="multipart/form-data">
                        <div class="modal-dialog">

                            <!-- Modal content-->
                            <div class="modal-content">
                                <div class="modal-header bg-primary text-white">
                                    Warning
                                </div>
                                <div class="modal-body">
                                    <p>Are you sure you want to delete this photo?</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="submit" class="btn btn-primary">Delete</button>
                                </div>
                            </div>

                        </div>
                    </form>
                </div>
            </div>

            <h4></h4>
            <c:if test='${!sessionScope.get("currentUser").equals(user)}'>
                <div class="btn-group" role="group">

                    <a class="btn btn-outline-secondary" role="button"
                            <c:choose>
                                <c:when test='${!isFriend}'>id="messageWarning"</c:when>
                                <c:otherwise>
                                    href="/user/${sessionScope.get("currentUser").getId()}/messages?userTo=${user.getId()}"
                                </c:otherwise>
                            </c:choose> > Write message</a>

                    <a class="btn btn-outline-secondary <c:if test='${isFriend||requestAlreadySent}'>disabled</c:if>"
                       role="button" id="sendFriendRequest">
                            ${isFriend ? "In your friend list" : "Add to Friends"}
                    </a>
                </div>
            </c:if>
        </div>

        <script>
            var userId = '${user.getId()}';
        </script>
        <script src="${pageContext.request.contextPath}/resources/js/common-scripts.js" type="text/javascript"></script>

        <div class="modal fade" id="friendWarning"
             tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">
                    <div class="modal-header bg-primary text-white">
                        Warning
                    </div>
                    <div class="modal-body">
                        Before writing messages add user to your friends and wait for accept
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">
                            OK
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-body">
            <h4>
                ${user.getFullName()}
            </h4>

            <fmt:formatDate var="fmtBirthday" value="${user.getBirthDay()}" pattern="d MMMM yyyy"/>
            <fmt:formatDate var="fmtRegDate" value="${user.getRegistrationDate()}" pattern="d MMMM yyyy 'at' H:mm"/>

            <table class="table table-sm table-borderless">
                <tbody>
                <tr class="row">
                    <td class="col-sm-2"><strong>E-Mail:</strong></td>
                    <td class="col-sm-4">${user.getEmail()}</td>
                </tr>
                <tr class="row">
                    <td class="col-sm-2"><strong>Birthday:</strong></td>
                    <td class="col-sm-4">${fmtBirthday}</td>
                </tr>
                <tr class="row">
                    <td class="col-sm-2"><strong>Phones:</strong></td>
                    <td class="col-sm-4">${phones}</td>
                </tr>
                <tr class="row">
                    <td class="col-sm-2"><strong>Skype:</strong></td>
                    <td class="col-sm-4">${user.getSkype()}</td>
                </tr>
                <tr class="row">
                    <td class="col-sm-2"><strong>ICQ:</strong></td>
                    <td class="col-sm-4">${user.getIcq()}</td>
                </tr>
                <tr class="row">
                    <td class="col-sm-2"><strong>Registration date:</strong></td>
                    <td class="col-sm-4">${fmtRegDate}</td>
                </tr>

                </tbody>
            </table>

            <h1></h1>
            <c:if test='${sessionScope.get("currentUser").equals(user)}'>
                <a class="btn btn-primary" href="/user/${user.getId()}/edit" role="button">Edit</a>
            </c:if>

        </div>
    </div>


    <%@include file="postsOnWall.jsp" %>

</div>
<!-- Main Col END -->

<%@include file="common/footer.jsp" %>