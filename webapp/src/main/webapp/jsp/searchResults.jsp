<%@include file="common/header.jsp" %>
<%@include file="common/sidebar.jsp" %>

<!-- MAIN -->
<div class="col py-3">

    <h5></h5>
    <h5>People</h5>

    <c:set var="pagetype" value="user"/>
    <div id="user-results">
        <c:forEach var="entity" items="${users}">
            <%@include file="common/searchResultCard.jsp" %>
        </c:forEach>
    </div>

    <c:choose>
        <c:when test='${usersPages == 0}'>
            No results found
        </c:when>
        <c:when test='${usersPages > 1}'>
            <nav aria-label="users pagination">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" id="page-user-prev">Previous</a></li>
                    <c:forEach var="i" begin="1" end="${usersPages}">
                        <li class="page-item" id="page-user-item${i}"><a class="page-link" id="user-page${i}">${i}</a>
                        </li>
                    </c:forEach>
                    <li class="page-item"><a class="page-link" id="page-user-next">Next</a></li>
                </ul>
            </nav>
        </c:when>
    </c:choose>

    <h5>Communities</h5>

    <c:set var="pagetype" value="group"/>
    <div id="group-results">
        <c:forEach var="entity" items="${groups}">
            <%@include file="common/searchResultCard.jsp" %>
        </c:forEach>
    </div>

    <c:choose>
        <c:when test='${groupPages==0}'>
            No results found
        </c:when>
        <c:when test='${groupPages > 1}'>
            <nav aria-label="groups pagination">
                <ul class="pagination">
                    <li class="page-item"><a class="page-link" id="page-group-prev">Previous</a></li>
                    <c:forEach var="i" begin="1" end="${groupPages}">
                        <li class="page-item" id="page-group-item${i}"><a class="page-link" id="group-page${i}">${i}</a>
                        </li>
                    </c:forEach>
                    <li class="page-item"><a class="page-link" id="page-group-next">Next</a></li>
                </ul>
            </nav>
        </c:when>
    </c:choose>

</div>

<script>
    var searchQuery = '${searchQuery}';
    var maxUserPageNumber = parseInt('${usersPages}');// java int not int in JS...
    var maxGroupPageNumber = parseInt('${groupPages}');// java int not int in JS...
</script>
<script src="${pageContext.request.contextPath}/resources/js/search-pages.js" type="text/javascript"></script>

<%@include file="common/footer.jsp" %>