<%@page contentType="text/html; charset=UTF-8" language="java"  %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
    <head>
        <title>Social network</title>
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="${pageContext.request.contextPath}/css/signin.css">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/favicon.png" type="image/png" />
    </head>
    <body>

        <div class="container">
            <div class="alert alert-danger">
                <h1>${errorText}</h1>
                ${errorDescription}
            </div>
        </div>

        <script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js" type="text/javascript"></script>
    </body>
</html>