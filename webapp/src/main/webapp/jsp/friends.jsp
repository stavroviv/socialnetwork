<%@include file="common/header.jsp" %>
<%@include file="common/sidebar.jsp" %>

<div class="col py-3">

    <h1> </h1>

    <ul class="nav nav-tabs" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="allFriends-tab" data-toggle="tab" href="#allFriends" role="tab"
               aria-controls="allFriends" aria-selected="true">
                All friends
            </a>
            <h1></h1>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="friendsRequest-tab" data-toggle="tab" href="#friendsRequest" role="tab"
               aria-controls="friendsRequest" aria-selected="false">
                Friend requests
                <c:if test='${usersFriendRequests.size() > 0}'>
                    <span class="badge badge-pill badge-primary ml-2">${usersFriendRequests.size()}</span>
                </c:if>
            </a>
        </li>
    </ul>

    <div class="tab-content">

        <div class="tab-pane fade show active" id="allFriends" role="tabpanel" aria-labelledby="allFriends-tab">
            <h1></h1>
            <c:set var="requestPage" value="false"/>
            <c:forEach var="user" items="${users}">
                <%@include file="common/friendUserCard.jsp" %>
            </c:forEach>
        </div>
        <div class="tab-pane fade" id="friendsRequest" role="tabpanel" aria-labelledby="friendsRequest-tab">
            <c:set var="requestPage" value="true"/>
            <c:forEach var="user" items="${usersFriendRequests}">
                <%@include file="common/friendUserCard.jsp" %>
            </c:forEach>
        </div>
    </div>

</div>

<%@include file="common/footer.jsp" %>