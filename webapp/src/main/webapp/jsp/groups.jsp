<%@include file="common/header.jsp" %>
<%@include file="common/sidebar.jsp" %>

<div class="col py-3">

    <h1></h1>
    <a class="btn btn-primary" href="/group_create" role="button">Create community</a>
    <h1></h1>

    <c:forEach var="group" items="${groups}">

        <div class="card flex-row flex-wrap">

            <div class="row no-gutters">
                <div class="col-auto">

                    <c:choose>
                        <c:when test="${group.getPicture() != null}">
                            <image src="data:image/jpg;base64,${group.getBase64Picture()}" width="150" height="150"/>
                        </c:when>
                        <c:otherwise>
                            <img src="${pageContext.request.contextPath}/resources/img/groups-default.jpg" width="150"
                                 height="150">
                        </c:otherwise>
                    </c:choose>

                </div>
                <div class="col">

                    <div class="card-block px-2">
                        <h1></h1>
                        <h4 class="card-title"><a href="/group/${group.getId()}/home"> ${group.getName()}</a></h4>
                        <p class="card-text">${group.getAddInfo().replaceAll("\\n","<BR>")}</p>
                        <h1></h1>
                    </div>

                </div>
            </div>

        </div>
        <h1></h1>

    </c:forEach>

</div>

<%@include file="common/footer.jsp" %>