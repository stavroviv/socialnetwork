<%@page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Social network</title>
    <link rel="shortcut icon" href="${pageContext.request.contextPath}/resources/img/favicon.png" type="image/png"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css" type="text/css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/resources/css/signin.css">
</head>
<body>

<div class="container">

    <form class="form-signin" method="post" action="<c:url value='/login'/>">

        <c:if test="${not empty error}">
            <div class="alert alert-danger">
                <c:out value='${error}'/>
            </div>
        </c:if>

        <label class="sr-only">Email</label>
        <input type="email" name="username" class="form-control" placeholder="Email" autofocus value="${username}">
        <label class="sr-only">Password</label>
        <input type="password" name="password" class="form-control" placeholder="Password" value="${password}">

        <div class="checkbox mb-3">
            <label><input type="checkbox" name="remember-me" checked> Remember me </label>
        </div>

        <button type="submit" class="btn btn-primary btn-block" name="actionName" value="signIn">Log in</button>

        <a class="btn btn-outline-secondary btn-block" href="registration" role="button">Register</a>

    </form>

</div>

<script src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js" type="text/javascript"></script>
</body>
</html>