<%@include file="common/header.jsp" %>
<%@include file="common/sidebar.jsp" %>

<!-- MAIN -->
<div class="col py-3">

    <form action="/group/${group.id == null ? 0 : group.id}/saveSettings" method="post" class="form-horizontal">
        <H2></H2>

        <%@include file="common/serviceMessages.jsp" %>

        <H2>${pageHeader}</H2>

        <div class="form-group row">
            <label class="col-sm-1 col-form-label">Name:</label>
            <div class="col-sm-6">
                <input name="name" class="form-control" id="name"
                       value = "${group.name}" onchange="validate()" required/>
                <div class="invalid-feedback">
                    Please enter group name
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-1 col-form-label">Description:</label>
            <div class="col-sm-6">
                <textarea name="addInfo" class="form-control" rows="10" required>${group.addInfo}</textarea>
            </div>
        </div>

        <button type="button" id="save-button" class="btn btn-primary" data-target="#save-settings" onclick="openSaveDialog()">
            Save
        </button>
        <%@include file="common/dialog.jsp" %>

         <c:if test="${pageHeader == 'Edit'}">
            <a class="btn btn-outline-secondary" href="/group/${group.getId()}/home" role="button">back to page</a>
         </c:if>
      </form>


</div>
<!-- Main Col END -->

<script>var formName='group'</script>
<script src="${pageContext.request.contextPath}/resources/js/validator.js" type="text/javascript"></script>
<%@include file="common/footer.jsp" %>