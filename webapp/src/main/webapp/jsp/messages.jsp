<%@include file="common/header.jsp" %>
<%@include file="common/sidebar.jsp" %>

<div class="col py-3">

    <h1></h1>

    <div class="row">

        <div class="col-3 overflow-auto" style="max-height: 80vh;">
            <div id="message-list" class="list-group">
                <c:forEach var="user" items="${friends}">
                    <a class="list-group-item list-group-item-action" id="user_messages_${user.getId()}">
                        <c:choose>
                            <c:when test="${user.getPicture() != null}">
                                <img class="rounded-circle"
                                     src="data:image/jpg;base64,${user.getBase64Picture()}"
                                     width="60" height="60"/>
                            </c:when>
                            <c:otherwise>
                                <img class="rounded-circle"
                                     src="${pageContext.request.contextPath}/resources/img/user-default.jpg"
                                     width="60" height="60">
                            </c:otherwise>
                        </c:choose>
                        &nbsp ${user.getName()} ${user.getFamilyName()}
                    </a>
                </c:forEach>

            </div>
        </div>

        <div class="col-8">

            <div class="messages" id="messages_from_user">
            </div>
            <p>
                    <textarea class="form-control" name="postText" rows="2"
                              id="messageText" placeholder="Write a message..."></textarea>
            </p>
            <button class="btn btn-primary" type="button" id="sendMessageButton" disabled>
                Send
            </button>

        </div>

    </div>

    <script>
        var curUser = ${sessionScope.currentUser.getId()};
        var userTo = ${userTo};
    </script>
    <script src="${pageContext.request.contextPath}/resources/js/messaging.js" type="text/javascript"></script>

</div>

<%@include file="common/footer.jsp" %>