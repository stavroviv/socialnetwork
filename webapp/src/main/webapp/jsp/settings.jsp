<%@include file="common/header.jsp" %>
<%@include file="common/sidebar.jsp" %>

<!-- MAIN -->
<div class="col py-3">

    <form action="/user/${user.getId()}/saveSettings" method="post" id="myForm" class="form-horizontal">
        <H2></H2>
        <%@include file="common/serviceMessages.jsp" %>

        <H4>Settings</H4>

        <div class="form-group row">
            <label class="col-sm-1 col-form-label">E-Mail:</label>
            <div class="col-sm-4">
                <input name="email" id="email" type="text" maxlength="255"
                       class="form-control" value="${user.email}" onchange="validate()" required>
                <div class="invalid-feedback">
                    Please enter your e-mail
                </div>
            </div>
        </div>

        <hr class="mb-4">

        <div class="form-group row">
            <label class="col-sm-1 col-form-label">First name:</label>
            <div class="col-sm-4">
                <input name="name" id="name" type="text" maxlength="255"
                       class="form-control" value="${user.name}" onchange="validate()" required>
                <div class="invalid-feedback">
                    Please enter your first name
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-1 col-form-label">Last name:</label>
            <div class="col-sm-4">
                <input name="familyName" id="familyName" type="text" maxlength="255"
                       class="form-control" value="${user.familyName}" onchange="validate()" required>
                <div class="invalid-feedback">
                    Please enter your last name
                </div>
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-1 col-form-label">Middle name:</label>
            <div class="col-sm-4">
                <input name="middleName" class="form-control" value="${user.middleName}">
            </div>
        </div>

        <hr class="mb-4">

        <div class="form-group row">
            <label class="col-sm-1 col-form-label">Skype:</label>
            <div class="col-sm-4">
                <input name="skype" class="form-control" value="${user.skype}">
            </div>
        </div>

        <div class="form-group row">
            <label class="col-sm-1 col-form-label">ICQ:</label>
            <div class="col-sm-4">
                <input name="icq" class="form-control" value="${user.icq}">
            </div>
        </div>

        <fmt:formatDate var="fmtBirthday" value="${user.birthDay}" pattern="yyyy-MM-dd"/>
        <div class="form-group row">
            <label class="col-sm-1 col-form-label">Birthday:</label>
            <div class="col-sm-4">
                <input name="birthDay" class="form-control" type="date" value="${fmtBirthday}">
            </div>
        </div>

        <c:set var="rowNumber" value="${0}"/>

        <hr class="mb-4">

        <div class="row">

            <div class="col-md-1">
                <label>Phones:</label>
            </div>

            <div class="col-md-6">
                <c:forEach var="phone" items="${phones}">
                    <c:set var="rowNumber" value="${rowNumber + 1}"/>

                    <div class="form-group row" id="phone${rowNumber}">
                        <div class="col-sm-8">
                            <input name="phones" class="form-control format-phone"
                                   value="${phone.toString()}" onchange="validate()" required>
                            <div class="invalid-feedback">
                                Please enter correct phone in format +7(xxx) xxx-xx-xx
                            </div>
                        </div>
                        <div class="col-sm-3 pl-sm-0">
                            <button type="button" style="height:38px" class="btn btn-outline-secondary"
                                    id="button_del${rowNumber}">
                                <i class="fa fa-trash-o fa-lg"></i>
                            </button>
                        </div>
                    </div>

                </c:forEach>

                <div class="divcls"></div>

                <div class="form-group row">
                    <div class="col-sm-4">
                        <button type="button" class="btn btn-outline-primary" id="button_add">
                            <i class="fa fa-plus-circle fa-lg"></i> Add
                        </button>
                    </div>
                </div>
            </div>
        </div>

        <hr class="mb-3">
        <script>var i = ${rowNumber};</script>
        <script src="${pageContext.request.contextPath}/resources/js/phone-editor.js" type="text/javascript"></script>

        <button type="button" id="save-button" class="btn btn-primary" data-target="#save-settings"
                onclick="openSaveDialog()">
            Save
        </button>
        <%@include file="common/dialog.jsp" %>

        <a class="btn btn-outline-secondary" href="/user/${user.getId()}/home" role="button">
            back to page
        </a>

    </form>

    <hr class="mb-2">

    <H4>Import, export user settings</H4>
    <H4></H4>
    <a class="btn btn-primary" href="/user/${user.getId()}/saveToXML" role="button">
        Save to XML file
    </a>

    <%--Modal upload from XML--%>
    <button type="button" class="btn btn-outline-secondary" data-toggle="modal" data-target="#loadFromXML">
        Load from XML file
    </button>

    <div class="container">
        <div class="modal fade" id="loadFromXML" role="dialog">
            <form action="/user/${user.getId()}/loadFromXML" method="post" enctype="multipart/form-data">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            Upload user data from XML
                        </div>
                        <div class="modal-body">
                            <p>Please, choose the file with *.xml extension</p>
                            <input type="file" name="xmlFile" accept=".xml">
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Upload</button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>

                </div>
            </form>
        </div>
    </div>

    <script>var formName = 'user'</script>
    <script src="${pageContext.request.contextPath}/resources/js/validator.js" type="text/javascript"></script>

</div>
<!-- Main Col END -->

<%@include file="common/footer.jsp" %>