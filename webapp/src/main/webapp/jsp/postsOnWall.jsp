<p>
<c:if test="${postOnWallMode == 'user'}">
    <form action="/user/${user.getId()}/sendPost" method="post" class="form-horizontal">
</c:if>
<c:if test="${postOnWallMode == 'group'}">
    <form action="/group/${group.getId()}/sendPost" method="post" class="form-horizontal">
</c:if>
<div class="form-group">
        <textarea class="form-control" name="postText" rows="3"
                  id="inputTextPost" placeholder="Add a post..."></textarea>
</div>
<button type="submit" id="sendButton" class="btn btn-primary" name="actionName" value="sendPost">Post</button>
</form>

<c:forEach var="post" items="${posts}">
    <div class="card">
        <div class="card-header">
            <a href="/user/${post.sender.id}/home">${post.sender.getFullName()}</a>
            <small>
                <div class="text-muted">
                    <fmt:formatDate value="${post.sendingDate}" pattern="d MMM yyyy 'at' H:mm"/>
                </div>
            </small>
        </div>
        <div class="card-body">
            <div class="card-text">${post.msgText.replaceAll("\\n","<BR>")}</div>
        </div>
    </div>
    <h1></h1>
</c:forEach>

<script type="text/javascript">
    document.getElementById('sendButton').setAttribute('disabled', true);
    var inputTextPost = document.getElementById('inputTextPost');
    inputTextPost.oninput = function () {
        if (inputTextPost.value === '') {
            document.getElementById('sendButton').setAttribute('disabled', true);
        } else {
            document.getElementById('sendButton').removeAttribute('disabled');
        }
    };
</script>

</p>
