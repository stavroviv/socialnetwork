var stompClient = null;
var userFrom;
var chat = $('#messages_from_user');
var sendMessageButton = $('#sendMessageButton');
var messageTextarea = $('#messageText');
var userMessages = $('[id^=user_messages]');

$(function () {

    if (userTo !== 0) {
        userFrom = userTo;
        activateChatRoom('user_messages_' + userTo);
        // remove parameter from URL
        var uri = window.location.toString();
        if (uri.indexOf('?') > 0) {
            var clean_uri = uri.substring(0, uri.indexOf('?'));
            window.history.replaceState({}, document.title, clean_uri);
        }
    }

    userMessages.click(function (event) {
        var element = event.currentTarget.id;
        userFrom = Number(element.replace('user_messages_', ''));
        activateChatRoom(element);
    });

    function activateChatRoom(element) {
        userMessages.removeClass('active');
        $('#' + element).addClass('active');
        chat.empty();
        loadMessages();
        connect(curUser, userFrom);
    }

    function loadMessages() {
        $.ajax({
            url: '/get-messages',
            data: {
                userFromId: userFrom,
                userToId: curUser
            },
            success: function (result) {
                chat.empty();
                $.each(result, function (index, value) {
                    showMessage(value);
                });
                scrollToLastMessage();
            }
        });
    }

    function scrollToLastMessage() {
        chat.animate(
            {scrollTop: chat.prop('scrollHeight')},
            500);
    }

    function disconnect() {
        if (stompClient !== null) {
            stompClient.disconnect();
        }
    }

    function connect(userFrom, userTo) {
        disconnect();// disconnect from previous chat
        var socket = new SockJS('/chat-messages');
        stompClient = Stomp.over(socket);
        stompClient.connect({}, function () {
            stompClient.subscribe('/message' + getChatRoom(userFrom, userTo), function (message) {
                showMessage(JSON.parse(message.body));
                scrollToLastMessage();
            });
        });
    }

    function getChatRoom(userFrom, userTo) {
        return '/' + (userFrom > userTo ? userFrom + '/' + userTo : userTo + '/' + userFrom);
    }

    function showMessage(message) {
        chat.append(
            '<div class="card">' +
            '<div class="card-header"><small>' + message.sender + ' ' + message.date + '</small></div>' +
            '<div class="card-body">' + message.text +
            '</div>' +
            '</div>' +
            '</div>' +
            '<h1></h1>'
        );
    }

    function sendMessage() {
        var messageText = messageTextarea.val();
        messageTextarea.val('');
        stompClient.send('/app/chat-messages' + getChatRoom(curUser, userFrom), {},
            JSON.stringify({'sender': curUser, 'userTo': userFrom, 'text': messageText}));
        sendMessageButton.attr('disabled', true);
    }

    sendMessageButton.click(function () {
        sendMessage();
    });

    messageTextarea.keydown(function (e) {
        if (e.ctrlKey && e.keyCode === 13) {
            sendMessage();
        }
    });

    messageTextarea.on('input', function () {
        if (messageTextarea.val() === '') {
            sendMessageButton.attr('disabled', true);
        } else {
            sendMessageButton.removeAttr('disabled');
        }
    });

});
