$(function () {

    $('#sendFriendRequest').click(function (event) {
        $.ajax({
            url: '/' + userId + '/sendFriendRequest',
            success: function (result) {
                $('#sendFriendRequest').addClass('disabled');
                $.notify('Friend request successfully sent', {
                    newest_on_top: true,
                    placement: {
                        from: 'bottom',
                        align: 'right'
                    }
                });
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(errorThrown);
            }
        });
    });

    $('#messageWarning').click(function (event) {
        $('#friendWarning').modal('show');
    });

});