var currentUserPageNumber = 1;
var currentGroupPageNumber = 1;
$('#page-user-item1').addClass('active');
$('#page-group-item1').addClass('active');

$(function () {
    $('[id^=page]').click(function (event) {
        var buttonName = event.target.id;
        var type;
        var curPageNumber;

        if (buttonName.indexOf('user') !== -1) {
            currentUserPageNumber = setPageNumber(currentUserPageNumber, event, maxUserPageNumber);
            curPageNumber = currentUserPageNumber;
            type = 'user';
        } else {
            currentGroupPageNumber = setPageNumber(currentGroupPageNumber, event, maxGroupPageNumber);
            curPageNumber = currentGroupPageNumber;
            type = 'group';
        }

        // set active element
        $('[id^=page-' + type + ']').removeClass('active');
        $('#page-' + type + '-item' + curPageNumber).addClass('active');

        callAjaxSearch(curPageNumber, type);
    });
});

function setPageNumber(currentPageNumber, event, maxPageNumber) {
    var buttonName = event.target.id;
    if (buttonName.indexOf('prev') !== -1) {
        if (currentPageNumber === 1) {
            currentPageNumber = 1;
        } else {
            currentPageNumber--;
        }
    } else if (buttonName.indexOf('next') !== -1) {
        if (currentPageNumber === maxPageNumber) {
            currentPageNumber = maxPageNumber;
        } else {
            currentPageNumber++;
        }
    } else {
        var bName = buttonName.replace('page', '').replace('user-', '').replace('group-', '');
        currentPageNumber = parseInt(bName);
    }
    return currentPageNumber;
}

function callAjaxSearch(pageNumber, type) {
    $.ajax({
        url: '/search_request',
        data: {
            query: searchQuery,
            page: pageNumber,
            type: type
        },
        success: function (result) {
            $('#' + type + '-results').empty();
            $.each(result, function (index, value) {
                outputResult(value, type);
            });
        }
    });
}

function outputResult(value, type) {
    var picture = '';
    if (value.PIC === '') {
        picture = 'resources/' + (type === 'user' ? 'img/user-default.jpg' : 'img/groups-default.jpg');
    } else {
        picture = 'data:image/jpg;base64,' + value.PIC;
    }
    $('#' + type + '-results').append(
        '<div class="card flex-row flex-wrap">' +
        '<div class="row no-gutters">' +
        '<div class="col-auto">' +
        '<a href="/' + type + '/' + value.id + '/home">' +
        '<img class="rounded-circle" src="' + picture + '" width="60" height="60">' +
        '</a>' +
        '</div>' +
        '<div class="col">' +
        '<div class="card-block px-2">' +
        '<h1></h1>' + value.name +
        '<p class="card-text"></p>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<h1></h1>');
}