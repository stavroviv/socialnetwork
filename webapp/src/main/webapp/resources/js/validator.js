function validate() {
    var result = true;
    var items;
    if (formName === 'user') {
        items = ['email', 'name', 'familyName'];
        document.querySelectorAll('.format-phone').forEach(function (item) {
            if (!validatePhoneNumber(item)) {
                result = false;
            }
        });
    } else if (formName === 'group') {
        items = ['name'];
    } else if (formName === 'registration') {
        items = ['name', 'familyName', 'email', 'password'];
    }
    items.forEach(function (item, arr) {
        if (!validateField(item)) {
            result = false;
        }
    });
    return result;
}

function validatePhoneNumber(item) {
    var phone = /^(\+7|7|8)?[\s\-]?\(?[0-9][0-9]{2}\)?[\s\-]?[0-9]{3}[\s\-]?[0-9]{2}[\s\-]?[0-9]{2}$/;
    var result = phone.test(item.value);
    if (result) {
        item.classList.remove('is-invalid');
    } else {
        item.classList.add('is-invalid');
    }
    return result;
}

function validateField(elementName) {
    var element = document.getElementById(elementName);
    var result = $('#' + elementName + '').val() !== '';
    if (result) {
        element.classList.remove('is-invalid');
    } else {
        element.classList.add('is-invalid');
    }
    return result;
}