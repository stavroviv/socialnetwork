var maskOptions = {mask: '+{7}(000) 000-00-00'};
document.querySelectorAll('.format-phone').forEach(function (item) {
    var mask = new IMask(item, maskOptions);
});

// document because of dynamic elements
$(document).on('click', '[id^=button]', function (event) {
    var buttonClicked = event.currentTarget.id;
    if (buttonClicked.indexOf('button_add') > -1) {
        addPhone();
    }
    if (buttonClicked.indexOf('button_del') > -1) {
        deletePhone(buttonClicked.replace('button_del', ''));
    }
});

function addPhone() {
    i++;
    $('.divcls').append(
        '<div class="form-group row" id="phone' + i + '">' +
        '<div class="col-sm-8">' +
        '<input name="phones" class="form-control format-phone" onchange="validate()" required>' +
        '<div class="invalid-feedback">Please enter correct phone in format +7(xxx) xxx-xx-xx</div>' +
        '</div>' +
        '<div class="col-sm-3 pl-sm-0">' +
        '<button type="button" style="height:38px" class="btn btn-outline-secondary" id="button_del' + i + '">' +
        '<i class="fa fa-trash-o fa-lg"></i>' +
        '</button>' +
        '</div>' +
        '</div>');
    document.querySelectorAll('.format-phone').forEach(function (item) {
        var mask = new IMask(item, maskOptions);
    })
}

function deletePhone(id) {
    $('#phone' + id).remove();
}