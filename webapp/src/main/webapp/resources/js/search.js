$((function () {

    var queryText;

    $.widget('custom.catcomplete', $.ui.autocomplete, {
        _create: function () {
            this._super();
            this.widget().menu('option', 'items', '> :not(.ui-autocomplete-category)');
        },
        _renderMenu: function (ul, items) {
            var that = this,
                currentCategory = '';
            $.each(items, function (index, item) {
                var li;
                if (item.category != currentCategory) {
                    ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
                    currentCategory = item.category;
                }
                li = that._renderItemData(ul, item);

                // -----> custom
                li.removeClass().addClass('search-result section-item');
                li.text('');
                var highlightedResult = String(item.label).replace(new RegExp(queryText, 'gi'),
                    '<span class="bg-info">$&</span>');
                var default_image = item.category == 'People'
                    ? '<img src="' + context + '/img/user-default.jpg' : '<img src="' + context + '/img/groups-default.jpg';
                var inner_html =
                    '<a href=""><div class="list_item_container">'
                    + '<div class="block">'
                    + (item.PIC == '' ?
                    default_image
                    : '<img  src="data:image/jpg;base64,' + item.PIC) + '" width="40" height="40" align="middle"></div>'
                    + '<div class="block">&nbsp' + highlightedResult + '</div></div>'
                    + '</a>';
                li.append(inner_html);
                // <----- custom

                if (item.category) {
                    li.attr('aria-label', item.category + ' : ' + item.label);
                }
            });
        }
    });

    $('input#autoText').catcomplete({
        delay: 0,
        source: function (request, response) {
            $.ajax({
                url: '/header_search_request',
                dataType: 'json',
                data: {
                    query: request.term
                },
                success: function (data, textStatus, jqXHR) {
                    var items = data;
                    response($.map(data, function (entry, i) {
                        return {
                            valueId: entry.id,
                            label: entry.name,
                            PIC: entry.PIC,
                            category: entry.category
                        }
                    }));

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    console.log(textStatus);
                }
            });
            queryText = request.term;
        },
        select: function (event, ui) {
            var path_to_page = (ui.item.category === 'People' ? 'user' : 'group');
            location.pathname = '../' + path_to_page + '/' + ui.item.valueId + '/home';
            event.preventDefault();
            $(this).val('');
            return false;
        },
        focus: function (event, ui) {
            return false;
        }
    });

}));