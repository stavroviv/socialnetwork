# Project "Social Network" 

**Functionality:**

+ registration 
+ display profile, groups
+ set settings for user and groups
+ import/export user settings from xml 
+ autocomplete (and pagination) search for users and groups 
+ adding/deleting friends 
+ chat messages between users 

**Tools:**
JDK 8, Spring (Security, MVC, WebSocket), Spring-boot, JPA / Hibernate, jQuery, JavaScript, 
Bootstrap, JUnit, Mockito, Maven, Git / Bitbucket, Tomcat, MySQL, IntelliJ IDEA.

**Screenshots**

![Login page](screenshots/login.png)  
___  

![Registration](screenshots/registration.png) 
___  
    
![Home page](screenshots/user-home.png)  
___ 

![Search](screenshots/search.png) 
___ 

![Friends](screenshots/friends.png) 
___ 

![Group page](screenshots/group.png)
___ 

![Chat](screenshots/chat.png) 
___ 

Demo application is hosted at [Heroku](https://soc-net-istavrov.herokuapp.com "Demobase")

Test account: test@gmail.com  
Password: qwerty  
___  

**Stavrov Ilia**  
Training getJavaJob  
http://www.getjavajob.com  