package com.getjavajob.training.stavrovi.socialnetwork.common;

public enum UserRole {
    USER, ADMIN
}
