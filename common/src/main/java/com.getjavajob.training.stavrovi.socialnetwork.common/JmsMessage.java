package com.getjavajob.training.stavrovi.socialnetwork.common;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

@Document(collection = "messages")
public class JmsMessage implements Serializable {
    private final static String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss.SSS";
    @Id
    public String id;
    private String text;
    private String timestamp;
    private String queueName;
    private String userTo;

    public JmsMessage() {

    }

    public JmsMessage(String text) {
        this.text = text;
        this.timestamp = dateToString(new Date());
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    private String dateToString(Date date) {
        SimpleDateFormat time_formatter = new SimpleDateFormat(DATE_FORMAT);
        return time_formatter.format(date);
    }

    public String getQueueName() {
        return queueName;
    }

    public void setQueueName(String queueName) {
        this.queueName = queueName;
    }

    @Override
    public String toString() {
        return "JmsMessage{" +
                "text='" + text + '\'' +
                ", timestamp=" + timestamp +
                '}';
    }

    public String getUserTo() {
        return userTo;
    }

    public void setUserTo(String userTo) {
        this.userTo = userTo;
    }
}
