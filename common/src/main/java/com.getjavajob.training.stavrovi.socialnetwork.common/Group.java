package com.getjavajob.training.stavrovi.socialnetwork.common;

import javax.persistence.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "GROUP_TABLE")
public class Group extends BaseEntity {
    @OneToOne
    @JoinColumn(name = "OWNER", referencedColumnName = "ID", nullable = false)
    private User creator;

    @ManyToMany(mappedBy = "userGroups")
    private List<User> members;

    public Group() {
        this.members = new LinkedList<>();
    }

    public Group(String name) {
        setName(name);
    }

    @Override
    public String toString() {
        return "" + getId() + " " + getName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Group group = (Group) o;
        return getName() == null || getName().equals(group.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName());
    }

    public User getCreator() {
        return creator;
    }

    public void setCreator(User creator) {
        this.creator = creator;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }

    public void addMember(User member) {
        members.add(member);
        if (!member.getUserGroups().contains(this)) {
            member.addGroup(this);
        }
    }

    public void removeMember(User member) {
        members.remove(member);
        if (member.getUserGroups().contains(this)) {
            member.removeGroup(this);
        }
    }
}
