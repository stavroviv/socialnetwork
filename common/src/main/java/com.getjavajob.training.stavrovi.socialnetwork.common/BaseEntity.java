package com.getjavajob.training.stavrovi.socialnetwork.common;

import com.thoughtworks.xstream.annotations.XStreamOmitField;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Base64;
import java.util.Date;

@MappedSuperclass
public abstract class BaseEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "ADD_INFO")
    private String addInfo;

    @XStreamOmitField
    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "REGISTRATION_DATE")
    private Date registrationDate;

    private String name;
    @XStreamOmitField
    private byte[] picture;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public byte[] getPicture() {
        return picture != null && picture.length == 0 ? null : picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public String getBase64Picture() {
        return picture == null || picture.length == 0 ? "" : Base64.getEncoder().encodeToString(picture);
    }

    public String getAddInfo() {
        return addInfo;
    }

    public void setAddInfo(String addInfo) {
        this.addInfo = addInfo;
    }

    public String getAddInfoWithHtml() {
        return addInfo.replaceAll(System.lineSeparator(), "<BR>");
    }
}
