package com.getjavajob.training.stavrovi.socialnetwork.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Where;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@XStreamAlias("User")
@Entity
@Table(name = "USER_TABLE")
public class User extends BaseEntity {
    private String email;
    private String icq;
    private String skype;

    @Column(name = "MIDDLE_NAME")
    private String middleName;

    @Column(name = "FAMILY_NAME")
    private String familyName;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "BIRTH_DAY")
    private Date birthDay;

    @Column(name = "PERSONAL_PHONE")
    private String personalPhone;

    @XStreamOmitField
    private String password;

    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "phone_table", joinColumns = @JoinColumn(name = "user_id"))
    @Column(name = "phone")
    @XStreamImplicit(itemFieldName = "Phone")
    private List<String> phones;

    @XStreamOmitField
    @OneToMany(mappedBy = "userTo", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    @Where(clause = "accepted=false")
    @Fetch(value = FetchMode.SUBSELECT)
    private List<FriendRequest> requestsToMe;

    @XStreamOmitField
    @ManyToMany
    @JoinTable(name = "GROUP_MEMBERS_TABLE",
            joinColumns = @JoinColumn(name = "MEMBER_ID", referencedColumnName = "ID"),
            inverseJoinColumns = @JoinColumn(name = "GROUP_ID", referencedColumnName = "ID"))
    private List<Group> userGroups;

    @Enumerated(EnumType.STRING)
    @XStreamOmitField
    private UserRole role;

    public User() {
        requestsToMe = new ArrayList<>();
    }

    public User(int id, String name, String familyName, String email) {
        setId(id);
        setName(name);
        this.familyName = familyName;
        this.email = email;
    }

    public User(int id, String name, String familyName) {
        this(id, name, familyName, "");
    }

    public User(String name, String familyName) {
        this(0, name, familyName);
    }

    public User(String name) {
        this(name, "");
    }

    public String getFullName() {
        return familyName + " " + getName() + (middleName == null ? "" : " " + middleName);
    }

    public String getShortName() {
        return familyName + " " + getName();
    }

    @Override
    public String toString() {
        return "" + getId() + " " + getShortName();
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getFamilyName() {
        return familyName;
    }

    public void setFamilyName(String familyName) {
        this.familyName = familyName;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthday) {
        this.birthDay = birthday;
    }

    public String getPersonalPhone() {
        return personalPhone;
    }

    public void setPersonalPhone(String personalPhone) {
        this.personalPhone = personalPhone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        User user = (User) o;
        return getId() == user.getId();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId());
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword() {
        return password;
    }

    public List<String> getPhones() {
        return phones;
    }

    public void setPhones(List<String> phones) {
        this.phones = phones;
    }

    public List<FriendRequest> getRequestsToMe() {
        return requestsToMe.stream()
                .filter(request -> !request.isAccepted())
                .collect(Collectors.toList());
    }

    public void setRequestsToMe(List<FriendRequest> requestsToMe) {
        this.requestsToMe = requestsToMe;
    }

    public void addRequest(FriendRequest request) {
        requestsToMe.add(request);
    }

    public void removeRequest(FriendRequest request) {
        requestsToMe.remove(request);
    }

    public List<Group> getUserGroups() {
        return userGroups;
    }

    public void setUserGroups(List<Group> userGroups) {
        this.userGroups = userGroups;
    }

    public void addGroup(Group group) {
        userGroups.add(group);
        if (!group.getMembers().contains(this)) {
            group.addMember(this);
        }
    }

    public void removeGroup(Group group) {
        userGroups.remove(group);
        if (group.getMembers().contains(this)) {
            group.removeMember(this);
        }
    }
}
