package com.getjavajob.training.stavrovi.socialnetwork.common.post;

import com.getjavajob.training.stavrovi.socialnetwork.common.User;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "USER_POSTS_TABLE")
public class UserPost extends GenericPost<User> {
    public UserPost() {

    }

    public UserPost(User sender, User recipient, String msgText) {
        super(sender, recipient, msgText);
    }
}
