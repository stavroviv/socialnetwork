package com.getjavajob.training.stavrovi.socialnetwork.common.post;

import com.getjavajob.training.stavrovi.socialnetwork.common.Group;
import com.getjavajob.training.stavrovi.socialnetwork.common.User;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "GROUP_POSTS_TABLE")
public class GroupPost extends GenericPost<Group> {
    public GroupPost() {

    }

    public GroupPost(User sender, Group recipient, String msgText) {
        super(sender, recipient, msgText);
    }
}
