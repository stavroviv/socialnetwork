package com.getjavajob.training.stavrovi.socialnetwork.common.post;

import com.getjavajob.training.stavrovi.socialnetwork.common.BaseEntity;
import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.util.Date;
import java.util.Objects;

@MappedSuperclass
public abstract class GenericPost<E extends BaseEntity> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "SENDER_ID")
    private User sender;

    @ManyToOne
    @JoinColumn(name = "RECIPIENT_ID")
    private E recipient;

    @Column(name = "MSG_TEXT")
    private String msgText;

    @CreationTimestamp
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "SENDING_DATE")
    private Date sendingDate;

    public GenericPost() {
    }

    public GenericPost(User sender, E recipient, String msgText) {
        this.sender = sender;
        this.recipient = recipient;
        this.msgText = msgText;
    }

    public Date getSendingDate() {
        return sendingDate;
    }

    public void setSendingDate(Date sendingDate) {
        this.sendingDate = sendingDate;
    }

    public String getMsgText() {
        return msgText;
    }

    public void setMsgText(String msgText) {
        this.msgText = msgText;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GenericPost<?> that = (GenericPost<?>) o;
        return sender.getId() == that.sender.getId() &&
                recipient.getId() == that.recipient.getId() &&
                Objects.equals(msgText, that.msgText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, msgText, sendingDate,
                sender == null ? null : sender.getId(),
                recipient == null ? null : recipient.getId());
    }

    public User getSender() {
        return sender;
    }

    public void setSender(User sender) {
        this.sender = sender;
    }

    public E getRecipient() {
        return recipient;
    }

    public void setRecipient(E recipient) {
        this.recipient = recipient;
    }
}
