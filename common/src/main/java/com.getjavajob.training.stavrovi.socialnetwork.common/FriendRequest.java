package com.getjavajob.training.stavrovi.socialnetwork.common;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "FRIENDS_TABLE")
public class FriendRequest
        // must implements Serializable since class with composite key is used as key itself
        implements Serializable {
    @Id
    @ManyToOne
    @JoinColumn(name = "USER_ID")
    private User userFrom;

    @Id
    @ManyToOne
    @JoinColumn(name = "FRIEND_ID")
    private User userTo;

    private boolean accepted;

    public FriendRequest() {
    }

    public FriendRequest(User userFrom, User userTo) {
        this.userFrom = userFrom;
        this.userTo = userTo;
    }

    public User getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(User userFrom) {
        this.userFrom = userFrom;
    }

    public User getUserTo() {
        return userTo;
    }

    public void setUserTo(User userTo) {
        this.userTo = userTo;
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FriendRequest that = (FriendRequest) o;
        return accepted == that.accepted &&
                Objects.equals(userFrom, that.userFrom) &&
                Objects.equals(userTo, that.userTo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userFrom, userTo, accepted);
    }
}
