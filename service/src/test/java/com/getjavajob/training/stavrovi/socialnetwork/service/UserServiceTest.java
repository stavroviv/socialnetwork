package com.getjavajob.training.stavrovi.socialnetwork.service;

import com.getjavajob.training.stavrovi.socialnetwork.common.FriendRequest;
import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import com.getjavajob.training.stavrovi.socialnetwork.dao.FriendRequestDao;
import com.getjavajob.training.stavrovi.socialnetwork.dao.user.UserDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
    @InjectMocks
    private UserService userService;
    @Mock
    private UserDao userDao;
    @Mock
    private User user;
    @Mock
    private FriendRequest friendRequest;
    @Mock
    private FriendRequestDao friendRequestDao;

    private User from;
    private User to;

    @Before
    public void init() throws Exception {
        from = new User(1, "test", "test", "test@mail.ru");
        to = new User(2, "test", "test", "test@mail.ru");
    }

    @Test
    public void getUserByIdTest() {
        int id = 1;
        when(userDao.getById(id)).thenReturn(user);
        assertEquals(user, userService.getUserById(id));
        verify(userDao, times(1)).getById(id);
    }

    @Test
    public void getUserByEMailTest() {
        String email = "test@mail.ru";
        when(userDao.getByEmail(email)).thenReturn(user);
        assertEquals(user, userService.getUserByEMail(email));
        verify(userDao, times(1)).getByEmail(email);
    }

    @Test
    public void createOrUpdateTest() throws ServiceException {
        when(userDao.save(user)).thenReturn(user);
        assertEquals(user, userService.createOrUpdate(user));
    }

    @Test
    public void getFriendsTest() {
        userService.sendFriendRequest(from, to);
        verify(friendRequestDao).sendRequest(from, to);
    }

    @Test
    public void sendFriendRequestTest() {
        List<User> friends = new LinkedList<>();
        friends.add(to);
        User user = mock(User.class);
        when(userDao.getFriends(user)).thenReturn(friends);
        assertEquals(userService.getFriends(user), friends);
    }

    @Test
    public void acceptFriendRequestTest() {
        userService.acceptFriendRequest(from, to);
        verify(friendRequestDao).acceptFriendRequest(from, to);
    }

    @Test
    public void declineFriendRequestTest() {
        userService.declineFriendRequest(from, to);
        verify(friendRequestDao).declineFriendRequest(from, to);
    }
}
