package com.getjavajob.training.stavrovi.socialnetwork.service;

import com.getjavajob.training.stavrovi.socialnetwork.common.Group;
import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import com.getjavajob.training.stavrovi.socialnetwork.common.post.GroupPost;
import com.getjavajob.training.stavrovi.socialnetwork.dao.GroupDao;
import com.getjavajob.training.stavrovi.socialnetwork.dao.post.GroupPostDao;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.times;

@RunWith(MockitoJUnitRunner.class)
public class GroupServiceTest {
    @InjectMocks
    private GroupService groupService;
    @Mock
    private GroupDao groupDao;
    @Mock
    private Group group;
    @Mock
    private GroupPostDao postDao;

    private Group testGroup;
    private User testUser;
    private GroupPost testPost;

    @Before
    public void init() {
        testUser = new User(1, "test", "test", "test@mail.ru");
        testGroup = new Group();
        testGroup.setName("Group name");
        testGroup.setCreator(testUser);
        testPost = new GroupPost(testUser, testGroup, "test wall post");
    }

    @Test
    public void getGroupByIdTest() {
        int id = 1;
        when(groupDao.getById(id)).thenReturn(group);
        assertEquals(group, groupService.getGroupById(id));
        verify(groupDao, times(1)).getById(id);
    }

    @Test
    public void getGroupByNameTest() {
        String name = "test";
        when(groupDao.getByName(name)).thenReturn(group);
        assertEquals(group, groupService.getGroupByName(name));
        verify(groupDao, times(1)).getByName(name);
    }

    @Test
    public void createOrUpdateTest() throws ServiceException {
        when(groupDao.save(testGroup)).thenReturn(testGroup);
        assertEquals(testGroup, groupService.createOrUpdate(testGroup));
    }

    @Test
    public void getGroupMembersTest() {
        List<User> members = new ArrayList<>();
        members.add(testUser);
        testGroup.setMembers(members);
        when(groupDao.getById(testGroup.getId())).thenReturn(testGroup);
        List<User> expected = new ArrayList<>(testGroup.getMembers());
        expected.add(testUser);
        assertEquals(expected, groupService.getGroupMembers(testGroup));
    }

    @Test
    public void addPostTest() {
        when(groupService.addPost(testPost)).thenReturn(true);
        assertEquals(true, groupService.addPost(testPost));
    }
}

