package com.getjavajob.training.stavrovi.socialnetwork.service;

import com.getjavajob.training.stavrovi.socialnetwork.common.Group;
import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import com.getjavajob.training.stavrovi.socialnetwork.common.post.GroupPost;
import com.getjavajob.training.stavrovi.socialnetwork.dao.GroupDao;
import com.getjavajob.training.stavrovi.socialnetwork.dao.post.GroupPostDao;
import com.getjavajob.training.stavrovi.socialnetwork.dao.user.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.LinkedList;
import java.util.List;

@Service
public class GroupService {
    @Autowired
    private GroupDao groupDao;
    @Autowired
    private UserDao userDao;
    @Autowired
    private GroupPostDao postDao;

    //region Common functions
    @Transactional
    public Group getGroupById(int id) {
        return groupDao.getById(id);
    }

    @Transactional
    public Group getGroupByName(String name) {
        return groupDao.getByName(name);
    }

    @Transactional
    public Group createOrUpdate(Group group) throws ServiceException {
        try {
            groupDao.save(group);
        } catch (Exception e) {
            throw new ServiceException("Error in updating group", e);
        }
        return group;
    }

    @Transactional
    public List<Group> getAllGroupsByQuery(String query) {
        return groupDao.getAllGroupsByQuery(query);
    }

    @Transactional
    public List<Group> getGroupsByQuery(String query, int page, int size) {
        return groupDao.getGroupsByQuery(query, PageRequest.of(page, size));
    }
    //endregion

    //region Membership
    @Transactional
    public void addMember(int groupId, User user) {
        groupDao.getById(groupId).addMember(userDao.getById(user.getId()));
        userDao.flush();
    }

    @Transactional
    public void removeMember(int groupId, User user) {
        groupDao.getById(groupId).removeMember(userDao.getById(user.getId()));
        userDao.flush();
    }

    @Transactional
    public List<User> getGroupMembers(Group group) {
        List<User> members = new LinkedList<>();
        members.add(group.getCreator());
        members.addAll(getGroupById(group.getId()).getMembers());
        return members;
    }

    @Transactional
    public List<Group> getUserGroups(User user) {
        List<Group> groups = new LinkedList<>();
        groups.addAll(userDao.getById(user.getId()).getUserGroups());
        groups.addAll(userDao.getUsersGroupsByOwner(user));
        return groups;
    }
    //endregion

    //region Wall posts
    @Transactional
    public boolean addPost(GroupPost post) {
        return postDao.addPost(post);
    }

    @Transactional
    public List<GroupPost> getPosts(Group recipient) {
        return postDao.getPosts(recipient);
    }
    //endregion

}
