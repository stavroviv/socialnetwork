package com.getjavajob.training.stavrovi.socialnetwork.service;

import com.getjavajob.training.stavrovi.socialnetwork.common.Message;
import com.getjavajob.training.stavrovi.socialnetwork.common.User;
import com.getjavajob.training.stavrovi.socialnetwork.common.post.UserPost;
import com.getjavajob.training.stavrovi.socialnetwork.dao.FriendRequestDao;
import com.getjavajob.training.stavrovi.socialnetwork.dao.MessageDao;
import com.getjavajob.training.stavrovi.socialnetwork.dao.post.UserPostDao;
import com.getjavajob.training.stavrovi.socialnetwork.dao.user.UserDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class UserService {
    @Autowired
    private UserDao userDao;
    @Autowired
    private UserPostDao postDao;
    @Autowired
    private MessageDao messageDao;
    @Autowired
    private FriendRequestDao friendRequestDao;

    //region Common functions
    @Transactional
    public User getUserById(int id) {
        return userDao.getById(id);
    }

    @Transactional
    public User getUserByEMail(String email) {
        return userDao.getByEmail(email);
    }

    @Transactional
    public User createOrUpdate(User user) throws ServiceException {
        try {
            userDao.save(user);
        } catch (Exception e) {
            throw new ServiceException("Error in updating user", e);
        }
        return user;
    }

    @Transactional
    public List<User> getAllUsersByQuery(String query) {
        return userDao.getAllUsersByQuery(query);
    }

    @Transactional
    public List<User> getUsersByQuery(String query, int page, int size) {
        return userDao.getUsersByQuery(query, PageRequest.of(page, size));
    }

    @Transactional
    public String getUserPhonesToString(User user) {
        return user.getPhones().stream()
                .map(Object::toString)
                .collect(Collectors.joining(", "));
    }
    //endregion

    //region Friends
    @Transactional
    public List<User> getFriends(User user) {
        return userDao.getFriends(user);
    }

    @Transactional
    public List<User> getUsersFriendRequests(User user) {
        return friendRequestDao.getUsersFriendRequests(user);
    }

    @Transactional
    public void sendFriendRequest(User userFrom, User userTo) {
        friendRequestDao.sendRequest(userFrom, userTo);
    }

    @Transactional
    public void acceptFriendRequest(User userFrom, User userTo) {
        friendRequestDao.acceptFriendRequest(userFrom, userTo);
    }

    @Transactional
    public void declineFriendRequest(User userFrom, User userTo) {
        friendRequestDao.declineFriendRequest(userFrom, userTo);
        // must update parent entity
//        try {
        userDao.save(userTo);
//        } catch (ServiceException e) {
//            e.printStackTrace();
//        }
    }
    //endregion

    //region Messages
    @Transactional
    public void sendMessage(User userFrom, User userTo, String text, Date date) {
        messageDao.sendMessage(userFrom, userTo, text, date);
    }

    @Transactional
    public List<Message> getMessages(User userFrom, User userTo) {
        return messageDao.getMessages(userFrom, userTo);
    }
    //endregion

    //region Wall posts
    @Transactional
    public boolean addPost(UserPost post) {
        return postDao.addPost(post);
    }

    @Transactional
    public List<UserPost> getPosts(User recipient) {
        return postDao.getPosts(recipient);
    }
    //endregion

}
